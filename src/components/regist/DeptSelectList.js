import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import {callDeptAllListAPI} from '../../apis/DeptAPICalls';

function DeptSelectList(props) {

	//   << props로 넘어오는 값들 >>
	// props.selectDept
	// -- 목록에서 선택한 deptCode, deptName을 받아서 부모 컴포넌트의 state인
	// -- selectedDeptCode와 selectedDeptName에 넣어준다.
	// 
	// props.close
	// -- 모달창을 닫고,
	// -- inputForm.deptCode와 inputForm.deptName에
	// -- selectedDeptCode와 selectedDeptName의 값을 넣어준다.

	const dispatch = useDispatch();
	const deptList = useSelector(state => state.deptReducer);
	const navigate = useNavigate();

	const [deptCode, setDeptCode] = useState({});

	useEffect(
		() => {
			dispatch(callDeptAllListAPI({

			}));
		}
		, []
	);

	const onClickTableTr = (deptCode, deptName) => {
		console.log("꼬꼬댁")
		// props.setSelectedDeptCode(deptCode);
		// props.setSelectedDeptName(deptName);
		props.selectDept(deptCode, deptName);
		// props.testfunction();
		props.close();
	}

	return (
		<div>
			<div>
				<div>
					<h4>직급선택</h4>
					{deptList &&
						<div>
							<table style={{ borderColor: '#aaaaaa', borderSpacing: 0 }} border={1}>
								<thead style={{ backgroundColor: '#266666', color: '#ffffff' }}>
									<tr>
										<th>부서코드</th>
										<th>부서명</th>
									</tr>
								</thead>
								<tbody>
									{Array.isArray(deptList) && deptList.map((dept) => (
										<tr key={dept.deptCode}>
											<td onClick={() => onClickTableTr(dept.deptCode, dept.deptName)}>{dept.deptCode}</td>
											<td onClick={() => onClickTableTr(dept.deptCode, dept.deptName)}>{dept.deptName}</td>
										</tr>
									))}
								</tbody>
							</table>
						</div>
					}
					<div>
						<button className='subButton'> 닫기 </button>
					</div>

				</div>
			</div>
		</div>
	);
}

export default DeptSelectList;