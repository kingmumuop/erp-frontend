import '../css/myCardStyle.css';
import face from '../images/face.png';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

function MyCard() {

  const navigate = useNavigate();
  const loginInfo = useSelector(state => state.loginReducer);

  const themeHandler = () => {
    navigate('/changeTheme')
  }

  const test = () => {
    console.log(loginInfo.empImage);
  }
  return (
    <div
      className='mycard'
      style={{
        backgroundColor:
          (loginInfo.empTheme === '초록') ? '#779999' :
            (loginInfo.empTheme === '파랑') ? '#B3C5D6' :
              (loginInfo.empTheme === '분홍') ? '#DBC8CB' :
                (loginInfo.empTheme === '노랑') ? '#EDDDB3' :
                  (loginInfo.empTheme === '주황') ? '#E7BE8C' :
                    (loginInfo.empTheme === '보라') ? '#8F80A1' :
                      (loginInfo.empTheme === '빨강') ? '#D6B3B3' :
                        '#779999'
      }}
    >
      <div className='top'>
        <div className='face'>
          {/* <img src={loginInfo.empImage} className='img' /> */}
          <img src={face} className='img' />
        </div>
        <div className='name'>
          <p>{loginInfo?.dept?.deptName}</p>
          <></>
          <p>{loginInfo.empName} {loginInfo?.position?.positionName}</p>
        </div>
      </div>
      <div className='bottom'>
        {(loginInfo.empName) ? <div>
          <button onClick={themeHandler}>테마 변경</button>
          <button onClick={() => navigate('/myPage')}>개인정보관리</button></div> :
          <p>로그인하세요</p>}
      </div>

      <button onClick={test}>테스트</button>
    </div>
  );
}

export default MyCard;