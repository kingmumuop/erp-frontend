import ApprovalStyle from '../../css/Approval.module.css';
import { useSelector } from 'react-redux';
import plus from '../../images/plus.png';
import { useState, useEffect } from "react";
import Modal from '../modal/Modal.js';
import EmpList from '../regist/EmpSelectList';
// import {decodeJwt} from '../../utils/tokenUtils';
import ApprovalLineList from './ApprovalLineList';

function ApprovalForm({ approvalLine, setApprovalLine, docType, isLinked, linkURL, setIsSelected, form, setForm, setAttachment }) {

  const [empModalOpen, setEmpModalOpen] = useState(false);
  const loginInfo = useSelector(state => state.loginReducer);
  let date = new Date();
  const today = date.toLocaleDateString();
  
  useEffect(
    () => {
      setForm({
        empCode: loginInfo.empCode,
        statusCode: 0,
        docType: docType,
        approvalTitle: '',
        approvalContent: '',
        attachment: '',
        link: '',
        vacationType: '',
        vacationStartDate: '',
        vacationEndDate: '',
      })
    }, [docType]
  );

  const onChangeFileUpload = (e) => {
    const attachment = e.target.files[0];
    setAttachment(attachment);
  };

  const onChangeHandler = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  /* 결재라인 추가 */
  const openApprovalLineModal = () => {
    setEmpModalOpen(true, { replace: false });
  }
  const closeEmpModal = () => {
    setEmpModalOpen(false);
  };

  const selectEmp = (chosenEmpCode, chosenEmpName) => {

    if(approvalLine.length === 0) {
      approvalLine.push(chosenEmpCode);
    } else {
      for (let i = approvalLine.length -1; i>= 0; i--) {
        if (approvalLine[i] === chosenEmpCode) {
          alert('동일한 코드가 있어요')
          approvalLine.splice(i, 1);
          continue;
        } 
      }
      approvalLine.push(chosenEmpCode);
    }
    console.log("선택된 결재자의 코드 : " + chosenEmpCode)
    console.log("선택된 결재자의 이름 : " + chosenEmpName)
  }

  const onTestHandler = () => {
    console.log(approvalLine)
  }

  // useEffect(
  //   () => {
  //     console.log('이상해')
  //   }, [approvalLine]
  // );


  return (
    <div className={ApprovalStyle.approvalForm}>

      <Modal open={empModalOpen} close={closeEmpModal} header='결재 라인 선택' >
        <EmpList selectEmp={selectEmp} close={closeEmpModal} />
      </Modal>

      <div>
        <h2>{docType}</h2>
      </div>

      <div className={ApprovalStyle.flexEnd}>
        <div className={ApprovalStyle.docTable}>
          <table border={1}>
            <tbody>
              <tr>
                <th>문서번호</th>
                <td></td>
              </tr>
              <tr>
                <th>부서</th>
                <td>{loginInfo?.dept?.deptName}</td>
              </tr>
              <tr>
                <th>기안일</th>
                <td>{today}</td>
              </tr>
              <tr>
                <th>기안자</th>
                <td>{loginInfo?.empName}</td>
              </tr>
              <tr>
                <th>직위</th>
                <td>{loginInfo?.position?.positionName}</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div className={ApprovalStyle.flex}>
          <div className={ApprovalStyle.stampHead}>
            <h4>결재</h4>
          </div>
          <div className={ApprovalStyle.horizontal} onClick={openApprovalLineModal}>
            <img src={plus} />
            <input type='text' value={approvalLine}/>
          </div>
          
        </div>
        {/* <p onClick={() => {console.log(approvalLine)}}>값: {approvalLine}</p> */}
      </div>

      <div className={ApprovalStyle.contentTable}>
        <table border={1} className={ApprovalStyle.contentT}>
          <tbody>
            <tr>
              <th>제목</th>
              <td colSpan={3}>
                <input type="text" name='approvalTitle' onChange={onChangeHandler} value={form.approvalTitle}></input>
              </td>
            </tr>
            {(docType === "휴가신청서") ?
              <>
                <tr><th>휴가종류</th><td colSpan={3}>
                  <div className={ApprovalStyle.sort}>
                    <div>
                      <input type="radio" id="day" name="vacationType" value="연차" onChange={onChangeHandler} />
                      <label for="day">연차</label>
                    </div>
                    <div>
                      <input type="radio" id="morning" name="vacationType" value="오전반차" onChange={onChangeHandler} />
                      <label for="morning">오전반차</label>
                    </div>
                    <div>
                      <input type="radio" id="afternoon" name="vacationType" value="오후반차" onChange={onChangeHandler} />
                      <label for="afternoon">오후반차</label>
                    </div>
                    <div>
                      <input type="radio" id="vacation" name="vacationType" value="기간휴가" onChange={onChangeHandler} />
                      <label for="vacation">기간휴가</label>
                    </div>
                  </div>
                </td></tr>
                <tr>
                  <th>시작일</th>
                  <td>
                    <input type="date" name='vacationStartDate' onChange={onChangeHandler} value={form.vacationStartDate} />
                  </td>
                  <th>종료일</th>
                  <td>
                    <input type="date" name='vacationEndDate' onChange={onChangeHandler} value={form.vacationEndDate} />
                  </td>
                </tr>
              </> :
              <tr>
                {((isLinked) && <th>링크</th>) || <th>첨부파일</th>}
                {((isLinked) && <td><input type="text" name='link' onChange={onChangeHandler} /></td>) ||
                  <td><input type='file' name='attachment' onChange={onChangeFileUpload} /></td>}
              </tr>
            }
            <tr>
              <th>내용</th>
              <td style={(docType === "휴가신청서") && { height: 150 } || { height: 180 }} colSpan={3} >
                <textarea name='approvalContent' onChange={onChangeHandler} className={ApprovalStyle.textArea} value={form.approvalContent} />
              </td>
            </tr>
            <tr>
              <td colSpan={4} className={ApprovalStyle.space}>상기와 같이 {docType}(을/를) 제출하오니 첨부파일 확인 부탁드립니다.</td>
            </tr>
          </tbody>
        </table>
      </div>


      <div className={ApprovalStyle.saveButton}>
        {/* <button onClick={draftHandler}>임시저장</button>
        <button onClick={saveHandler}>저장</button>
        <button onClick={() => setIsSelected(false)}>닫기</button> */}
        <button onClick={onTestHandler}>찐테</button>
      </div>
    </div>
  );
}

export default ApprovalForm;