import ApprovalStyle from '../../css/Approval.module.css';
import clip from '../../images/clip.png';
import { startTransition, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { callApprovalLineAPI } from '../../apis/ApprovalLineAPICalls';

function ApprovalTable({ approvalList, setIsLineClicked, isApprover }) {

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const loginInfo = useSelector(state => state.loginReducer);
  const approvalLines = useSelector(state => state.approvalLineReducer);

  /* 체크 선택 핸들러 */
  const onSelectHandler = (e) => {
    const checkbox = document.getElementsByClassName('check');
    for (var i = checkbox.length - 1; i >= 0; i--) {
      if (e.target.checked === true) {
        checkbox[i].checked = true;
      } else {
        checkbox[i].checked = false;
      }
    }
  };

  /* 행 클릭 핸들러 */
  const onLineClickHandler = (approvalCode) => {
    setIsLineClicked(true);
    {
      dispatch(callApprovalLineAPI({
        approvalCode: approvalCode
      }))
    }
  };

  /* 날짜 형식 */
  function formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = (d.getFullYear()) % 2000;
    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;
    return [year, month, day].join('-');
  };

  /* 승인 페이지로 이동 */
  const onSignOffHandler = (code) => {
    navigate(`/task/approval/signoff/${code}`, { replace: false })
  }

  /* 상세페이지로 이동 */
  const onDetailHandler = (code, status, emp) => {
    console.log(status);
    if (status.statusDesc === '최종승인' || emp === loginInfo.empCode || isApprover) {             // 추후 token 번호로 바꿔줘야 함
      navigate(`/task/approval/detail/${code}`, { replace: false })
    } else {
      alert('열람이 제한됩니다.')
    }
  };

  return (
    <div>
      <table border={1} className={ApprovalStyle.approvalTable}>
        <thead>
          <tr>
            <th style={{ width: 50 }}>
              <input
                type='checkbox'
                onChange={onSelectHandler}
                className='check'
              /></th>
            <th>기안일시</th>
            <th>구분</th>
            <th style={{borderRight: 'none', width: 300 }}>제목</th><th style={{borderLeft:'none', width: 20}} />
            <th>기안부서</th>
            <th>기안자</th>
            <th>상태</th>
            <th className={ApprovalStyle.docNo}>문서번호</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(approvalList) && approvalList.map((approval) => (
            <tr
              key={approval.approvalCode}
              onClick={() => onLineClickHandler(approval.approvalCode)}
              className={ApprovalStyle.approvalTr}>
              <td>
                <input type='checkbox' className='check' />
              </td>
              <td>
                {formatDate(approval.approvalDate)}
              </td>
              <td>
                {approval.docType}
              </td>
              {
                (isApprover) ? 
                <td 
                className={ApprovalStyle.approvalTile}
                onClick={() => onSignOffHandler(approval.approvalCode)}
                style={{ borderRight: 'none' }}>
                {approval.approvalTitle}
              </td> :
              <td
              className={ApprovalStyle.approvalTile}
              onClick={() => onDetailHandler(approval.approvalCode, approval.approvalStatus, approval.emp.empCode)}
              style={{ borderRight: 'none' }}>
              {approval.approvalTitle}
            </td>
              }

              <td
                style={{ borderLeft: 'none' }}>
                {
                  (approval.oldFileName !== null) ?
                    <img src={clip} style={{ width: 15, height: 15 }} /> : ''
                }
              </td>
              <td>
                {approval.emp?.dept?.deptName}
              </td>
              <td>
                {approval.emp?.empName}
              </td>
              <td>
                {approval.approvalStatus?.statusDesc}
              </td>
              <td>
                {approval.approvalCode}
              </td>
            </tr>
          ))
          }
        </tbody>
      </table>

      
    </div>
  );
}

export default ApprovalTable;