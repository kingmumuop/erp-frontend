import ApprovalLineStyle from '../../css/ApprovalLineList.module.css';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import checked from '../../images/checked.png';

function ApprovalLineList() {

  const approvalLines = useSelector(state => state.approvalLineReducer);

  return (
    <div className={ApprovalLineStyle.approvalLineList}>
      <table border={1} className={ApprovalLineStyle.lineTable}>
        <tbody>
          <tr>
            <th style={{width: 50}}>순서</th>
            <th>성명</th>
            <th>직책</th>
            <th>부서</th>
            <th style={{width: 40}}>상태</th>
            <th style={{width: 180}}>결재일시</th>
          </tr>
          { Array.isArray(approvalLines) && approvalLines.map(( line ) => (
            <tr            >
              <td>{ line.approverOrder }</td>
              <td>{ line.emp.empName }</td>
              <td>{ line.emp.position.positionName}</td>
              <td>{ line.emp.dept.deptName}</td>
              {/* <td>{ line.approveYn }</td> */}
              <td>{(line.approveYn === "Y") && <img src={checked} className={ApprovalLineStyle.checkImg}/>}</td>
              <td>{ line.approvedDate }</td>
            </tr>
          ))
          }
        </tbody>
      </table>
    </div>
  );
}

export default ApprovalLineList;