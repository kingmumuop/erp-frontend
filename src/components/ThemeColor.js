import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from "react";
import { callLoginInfoAPI } from '../apis/LoginAPICalls';
import { decodeJwt } from '../utils/tokenUtils';
import styled from 'styled-components';

function MainColor() {

  useEffect(
    () => {
      dispatch(callLoginInfoAPI({
        empCode: 1
      }));
    }, []
  )
  
  const dispatch = useDispatch();
  const loginInfo = useSelector(state => state.loginReducer);
  
    switch(loginInfo.empTheme) {
      case "빨강": return "#881111";
      case "주황": return "#DA8555";
      case "노랑": return "#DDAC2D";
      case "초록": return "#226666";
      case "파랑": return "#115588";
      case "보라": return "#421A83";
      case "분홍": return "#965A5A";
      default: return "#000000";
    }
  }

function SubColor() {

  useEffect(
    () => {
      dispatch(callLoginInfoAPI({
        empCode: 1
      }));
    }, []
  )

  const dispatch = useDispatch();
  const loginInfo = useSelector(state => state.loginReducer);

    switch(loginInfo.empTheme) {
      case "빨강": return "#F5F1F1";
      case "주황": return "#E9E5E2";
      case "노랑": return "#F1EFE6";
      case "초록": return "#E8EBE7";
      case "파랑": return "#EAEDF0";
      case "보라": return "#E0DDE8";
      case "분홍": return "#F2EFEF";
      default: return "#ffffff";
    }
}

export function FormatDate(date) {
  var
    d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = (d.getFullYear())%2000;
  if (month.length < 2) 
    month = '0' + month;
  if (day.length < 2) 
    day = '0' + day;
  return [year, month, day].join('/');
}

const FormattedNumber = styled.div`
    text-align: right;
`;
export function FormatNumber(number) {
  if (isNaN(number)) {
      return '';
  }
  const numberFormat = new Intl.NumberFormat('ko-KR');
  return <FormattedNumber>{numberFormat.format(number)}</FormattedNumber>;
}


export const Table = styled.table`
  font-size: 14px;
  text-align: center;
  margin: 1px;
  border-collapse: collapse;
  border-color: ${props => MainColor()};
  tr.border-thick { padding: 5px; border: 1px solid; border-color: ${props => MainColor()}};
  th {
    padding: 5px;
    background-color: ${props => MainColor()};
    color: white;
    border-inline-style: solid;
    border-inline-color: white;
    box-sizing: border-box;
    border-top-width: 1px;
    border-right-width: 1px;
    border-bottom-width: 1px;
    border-left-width: 1px;
  }
  td {
    padding: 5px;
    border: 1px solid; border-color: ${props => MainColor()}
  }
  
  td input {
    border: none;
  }

  td input[type="text"] {
    width: 100%;
    border: none;
  }

  td input[type="number"] {
    width: 80%;
    border: none;
    text-align: center;
  }

  th:first-child {
    border-left-width: 1px;
    border-left-style: solid;
    border-left-color: ${props => MainColor()}};
  }
  th:last-child {
    border-right-width: 1px;
    border-right-style: solid;
    border-right-color: ${props => MainColor()}};
  }

  &:hover {
    cursor: pointer;
  `;



export const Div = styled.div`
  background-color: ${props => SubColor()};}
  display: flex;
  margin-bottom: 15px;
`;


export const MainButton = styled.button`
margin: 1px;
height: 24px;
font-size: 14px;
border: 1px solid;
background-color: ${props => MainColor()};
border-color: ${props => MainColor()};
color: white;
align-self: flex-start;
&:hover {
  cursor: pointer;
}
`;

export const SubButton = styled.button`
margin: 1px;
height: 24px;
font-size: 14px;
border: 1px solid;
border-color: ${props => MainColor()};
background-color: ${props => SubColor()};
&:hover {
  cursor: pointer;
}
`;

export const Input = styled.input`
margin: 1px;
color: black;
border: 1px solid ${props => MainColor()};
font-size: 14px;
height: 21px;

::placeholder {
color: black;
}
`;


export const ModalDiv = styled.div`
  display: none;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 99;
  background-color: rgba(0, 0, 0, 0);

  button {
    cursor: pointer;
  }

  > section {
    width: 90%;
    max-width: 450px;
    margin: 0 auto;
    background-color: #fff;
    animation: modal-show 0.3s;
    overflow: hidden;

    > header {
      border: 1px solid ${props => MainColor()};
      height: 2vh;
      position: relative;
      padding: 16px 64px 16px 16px;
      background-color:  ${props => MainColor()};
      color: white;
      font-weight: bold;

      button {
        position: absolute;
        top: 15px;
        right: 15px;
        width: 30px;
        font-size: 21px;
        font-weight: 700;
        text-align: center;
        color: #999;
        background-color: transparent;
        border: 0px;
      }
    }

    > main {
      padding: 16px;
      border-left: 1px solid ${props => MainColor()};
      border-right: 1px solid ${props => MainColor()};
    }

    > footer {
      border-left: 1px solid ${props => MainColor()};
      border-right: 1px solid ${props => MainColor()};
      border-bottom: 1px solid ${props => MainColor()};
      padding: 12px 16px;
      text-align: right;

      button {
        height: 24px;
        font-size: 14px;
        border: 1px solid;
        background-color: ${props => MainColor()};
        border-color: ${props => MainColor()};
        color: white;
      }
    }
  }

  &.openModal {
    display: flex;
    align-items: center;
    animation: modal-bg-show 0.3s;
  }

  @keyframes modal-show {
    from {
      opacity: 0;
      margin-top: -50px;
    }
    to {
      opacity: 1;
      margin-top: 0;
    }
  }

  @keyframes modal-bg-show {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;
