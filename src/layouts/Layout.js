import Header from '../components/Header';
import { Outlet } from 'react-router-dom';
import Navbar from '../navbar/Navbar';
import '../css/layoutStyle.css';

function Layout() {
	return (
		<>
			<Header/>
			<div className='flex'>
				<Navbar/>
				<Outlet/>
			</div>
		</>
	);
}

export default Layout;