import Header from '../components/Header';
import { Outlet } from 'react-router-dom';
import MyCard from '../components/MyCard';
import '../css/layoutStyle.css';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {callLoginInfoAPI} from '../apis/LoginAPICalls';

function MainLayout() {

	const dispatch = useDispatch();

	useEffect(
    () => {
      // (token.sub) &&
      dispatch(callLoginInfoAPI({
        // empCode: token.sub
        empCode: 1
      }));
    }
    ,[]
  );

	return (
		<>
			<Header/>
			<div className='flex'>
				<MyCard/>
				<Outlet/>
			</div>
		</>
	);
}

export default MainLayout;