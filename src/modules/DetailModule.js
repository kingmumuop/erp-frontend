import { createActions, handleActions } from "redux-actions";

const initialState = [];
export const GET_STORAGE     = 'storage/GET_STORAGE';

const actions = createActions({
  [GET_STORAGE]: () => {}
});

const detailReducer = handleActions(
  { [GET_STORAGE]: (state, { payload }) => { return payload; } }, initialState);

export default detailReducer;

