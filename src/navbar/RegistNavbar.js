import '../css/navbarStyle.css'
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

function RegistNavbar() {

  const loginInfo = useSelector(state => state.loginReducer);
  let color = loginInfo.empTheme;

  return (
    <div className='navbar'
      style={
        {
          backgroundColor:
            (color === '초록') ? '#E8EBE7' :
              (color === '파랑') ? '#EAEDF0' :
                (color === '분홍') ? '#F2EFEF' :
                  (color === '노랑') ? '#F1EFE6' :
                    (color === '주황') ? '#E9E5E2' :
                      (color === '보라') ? '#E0DDE8' :
                      (color === '빨강') ? '#F5F1F1' :
                        '#E8EBE7'
        }
      }>

      <div className='link'>
        <h3>등록<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/regist/emp/list"      >사원등록</NavLink><br/>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/regist/dept/list"     >부서등록</NavLink><br/>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/regist/position/list" >직급등록</NavLink><br/>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/regist/product/list"  >품목등록</NavLink><br/>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/regist/storage/list"  >창고등록</NavLink><br/>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/regist/client/list"   >거래처등록</NavLink><br/>
      </div>

    </div>
  );
}

export default RegistNavbar;