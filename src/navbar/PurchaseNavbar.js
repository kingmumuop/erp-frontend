import '../css/navbarStyle.css'
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

function PurchaseNavbar() {

  const loginInfo = useSelector(state => state.loginReducer);
  let color = loginInfo.empTheme;

  return (
    <div className='navbar'
      style={
        {
          backgroundColor:
            (color === '초록') ? '#E8EBE7' :
              (color === '파랑') ? '#EAEDF0' :
                (color === '분홍') ? '#F2EFEF' :
                  (color === '노랑') ? '#F1EFE6' :
                    (color === '주황') ? '#E9E5E2' :
                      (color === '보라') ? '#E0DDE8' :
                      (color === '빨강') ? '#F5F1F1' :
                        '#E8EBE7'
        }
      }>

      <div className='link'>
        <h3>요청<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/purchase/request/list">요청서조회</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/purchase/request/regist">요청서입력</NavLink><br />
      </div>

      <div className='link'>
        <h3>발주<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/purchase/place/list">발주서조회</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/purchase/place/regist">발주서입력</NavLink><br />
      </div>

      <div className='link'>
        <h3>구매<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/purchase/purchase/list">구매조회</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/purchase/purchase/regist">구매입력</NavLink><br />
      </div>

      <div className='link'>
        <h3>재고<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/stock/list">재고조회</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/stock/storage">창고별재고</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/stock/detail">재고수불부</NavLink><br />
      </div>

    </div>
  );
}

export default PurchaseNavbar;