import '../css/navbarStyle.css'
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

function TaskNavbar() {

  const loginInfo = useSelector(state => state.loginReducer);
  let color = loginInfo.empTheme;

  return (
    <div className='navbar'
      style={
        {
          backgroundColor:
            (color === '초록') ? '#E8EBE7' :
              (color === '파랑') ? '#EAEDF0' :
                (color === '분홍') ? '#F2EFEF' :
                  (color === '노랑') ? '#F1EFE6' :
                    (color === '주황') ? '#E9E5E2' :
                      (color === '보라') ? '#E0DDE8' :
                      (color === '빨강') ? '#F5F1F1' :
                        '#E8EBE7'
        }
      }>


      <div className='link'>
        <h3>일정<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/scheduleList">일정조회</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/scheduleRegist">일정등록</NavLink><br />
      </div>

      <div className='link'>
        <h3>전자결재<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/approval/regist"  >기안서작성</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/approval/mylist"  >결재관리</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/approval/waiting" >대기중인결재</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/approval/storage" >임시저장문서</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/approval/stamp"   >도장등록</NavLink><br />
      </div>

      <div className='link'>
        <h3>게시판<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/board/list">게시판보기</NavLink><br />
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/board/regist">게시판등록</NavLink><br />
      </div>

      <div className='link'>
        <h3>업무<hr /></h3>
        <NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/task/todo/List">업무조회</NavLink><br />
      </div>

    </div>
  );
}

export default TaskNavbar;