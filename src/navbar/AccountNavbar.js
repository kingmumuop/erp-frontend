import '../css/navbarStyle.css'
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

function AccountNavbar() {

	const loginInfo = useSelector(state => state.loginReducer);
	let color = loginInfo.empTheme;

	return (
    <div className='navbar'
      style={
        {
          backgroundColor:
            (color === '초록') ? '#E8EBE7' :
              (color === '파랑') ? '#EAEDF0' :
                (color === '분홍') ? '#F2EFEF' :
                  (color === '노랑') ? '#F1EFE6' :
                    (color === '주황') ? '#E9E5E2' :
                      (color === '보라') ? '#E0DDE8' :
												(color === '빨강') ? '#F5F1F1' :
                        '#E8EBE7'
        }
      }>

			<div className='link'>
				<h3>입금<hr /></h3>
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/account/depositList">입금내역조회</NavLink><br />
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/account/depositRegist">입금내역입력</NavLink><br />
			</div>

			<div className='link'>
				<h3>출금<hr /></h3>
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/account/withdrawList">출금내역조회</NavLink><br />
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/account/withdrawRegist">출금내역입력</NavLink><br />
			</div>

		</div>
	);
}

export default AccountNavbar;