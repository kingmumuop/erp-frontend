import '../css/navbarStyle.css';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

function BusinessNavbar() {

	const loginInfo = useSelector(state => state.loginReducer);
	let color = loginInfo.empTheme;

	return (
    <div className='navbar'
      style={
        {
          backgroundColor:
            (color === '초록') ? '#E8EBE7' :
              (color === '파랑') ? '#EAEDF0' :
                (color === '분홍') ? '#F2EFEF' :
                  (color === '노랑') ? '#F1EFE6' :
                    (color === '주황') ? '#E9E5E2' :
                      (color === '보라') ? '#E0DDE8' :
												(color === '빨강') ? '#F5F1F1' :
                        '#E8EBE7'
        }
      }>

			<div className='link'>
				<h3>견적<hr /></h3>
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/business/estimate/list">견적서조회</NavLink><br />
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/business/estimate/regist">견적서등록</NavLink><br />
			</div>

			<div className='link'>
				<h3>주문<hr /></h3>
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/business/orders/list">주문서조회</NavLink><br />
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/business/orders/regist">주문서입력</NavLink><br />
			</div>

			<div className='link'>
				<h3>판매<hr /></h3>
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/business/sales/list">판매조회</NavLink><br />
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/business/sales/regist">판매입력</NavLink><br />
			</div>

			<div className='link'>
				<h3>재고<hr /></h3>
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/stock/list">재고조회</NavLink><br />
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/stock/storage">창고별재고</NavLink><br />
				<NavLink className={({ isActive }) => isActive ? "active" : "inactive"} to="/stock/detail">재고수불부</NavLink><br />
			</div>

		</div>
	);
}

export default BusinessNavbar;