import '../../../css/DeptRegist.css'
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { callDeptRegistAPI, callDeptCodesAPI } from '../../../apis/DeptAPICalls';

function DeptRegist() {

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const deptCodes = useSelector(state => state.deptReducer);
  const lastNum = Math.max.apply(null,deptCodes) + 1;

  useEffect(        
		() => {
			dispatch(callDeptCodesAPI()); 
		}
	,[]);

  const [form, setForm] = useState({
    deptCode: 0,
    deptName: '',
    deptSalary: 0
  });
  
  const onChangeHandler = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const onDeptSaveHandler = () => {
    console.log('[DeptRegist] onDeptSaveHandler')

    
    if(!form.deptCode || !form.deptName || !form.deptSalary) {
      alert('값을 모두 입력해주세요.');
      return false;
    }

    if(deptCodes.includes(Number(form.deptCode))) {
      const answer = window.confirm('이미 존재하는 부서코드입니다. 수정으로 진행할까요?')
      if(answer !== true) {
        return false;
      }
    }
    const formData = new FormData();
    formData.append("deptCode", form.deptCode);
    formData.append("deptName", form.deptName);
    formData.append("deptSalary", form.deptSalary);
    dispatch(callDeptRegistAPI({
      form: formData
    }));
    alert('부서 등록 완료');
    navigate('/regist/dept/list', {replace:true});
  }

  return (
    <div className="deptRegist">
      <h4>부서등록</h4>
      <table>
        <tbody>
          <tr>
            <td>부서코드</td>
            <td>
              <input 
                style={{height: 25}}
                type='number' 
                name='deptCode'
                placeholder={lastNum}
                onChange={ onChangeHandler }
              />
            </td>
          </tr>
          <tr>
            <td>부서명</td>
            <td>
              <input 
                style={{height: 25}}
                type='text'
                name='deptName'
                onChange={ onChangeHandler }
              />
            </td>
          </tr>
          <tr>
            <td>부서수당</td>
            <td>
              <input 
                style={{height: 25}}
                type='number'
                name='deptSalary'
                onChange={ onChangeHandler }
              />
            </td>
          </tr>
        </tbody>
      </table>
      <div className='button1'>
        <button onClick={onDeptSaveHandler}>등록</button>
        <button onClick={()=> navigate(-1)}>취소</button>
      </div>
    </div>
  );
}

export default DeptRegist;