import '../../../css/DeptList.css'
import queryString from 'query-string';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import back from '../../../images/back.png'
import {callSearchDeptAPI} from '../../../apis/DeptAPICalls'

function Search() {

  const navigate = useNavigate();
  const { search } = useLocation();
  const { value } = queryString.parse(search);
  const depts = useSelector(state => state.deptReducer); 
  const dispatch = useDispatch();
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    dispatch(callSearchDeptAPI({
      search: value
    }));        
  },
  []);

  /* 체크 선택 핸들러 */
  const onSelectHandler = (e) => {
    const input = document.getElementsByTagName('input');
    for(var i = input.length-1; i >= 0; i--) {
      if(input[0].checked === true) {
        input[i].checked = true;
      } else {
        input[i].checked = false;
      }
    }
  }

  /* 부서코드 클릭 핸들러 */
  const onClickTableTr = (deptCode) => {
		navigate(`/regist/dept/detail/${deptCode}`, { replace : false });
	}

  // const onSearchChangeHandler = (e) => {
  //   setSearchValue(e.target.value);
  // }

  // const onSearchHandler = () => {
  //   navigate(`/regist/dept/search?value=${search}`, { replace: false });
  //   window.location.reload();
  // }

  // const onEnterkeyHandler = (e) => {
	// 	if (e.key == 'Enter') {
  //     console.log('Enter key', search);
  //     navigate(`/regist/dept/search?value=${search}`, { replace: false });
  //     window.location.reload();
  // }
  // }

  return (
    <div className='outlet'>

      <h4>검색결과</h4>
      <div className='right' onClick={()=> navigate(-1)}>
        <p>돌아가기</p>
        <img src={back} className='imgButton'/>
      </div>


      <table border={1} className='deptTable'>
        <thead>
          <tr>
            <th><input type='checkbox' onChange={onSelectHandler}/></th>
            <th>부서코드</th>
            <th>부서명</th>
            <th>부서수당</th>
          </tr>
        </thead>
			  <tbody>
          { depts.length > 0 && depts.map(( dept ) => (
            <tr
              key={ dept.deptCode }
            >
              <td><input type='checkbox'/></td>
              <td onClick = { () => onClickTableTr( dept.deptCode )} className='deptCode'><a href=''>{ dept.deptCode }</a></td>
              <td>{ dept.deptName }</td>
              <td>{ dept.deptSalary}</td>
            </tr>
          ))
          }
				</tbody>
			</table>
    </div>
  );
}

export default Search;