import '../../../css/DeptRegist.css'
import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { callDeptDetailAPI, callDeptModifyAPI, callDeptDeleteAPI } from '../../../apis/DeptAPICalls';

function DeptDetail() {

  const dispatch = useDispatch(); 
  const navigate = useNavigate();
  const params = useParams();
  const dept = useSelector(state => state.deptReducer);
  const [form, setForm] = useState({});

  useEffect(        
		() => {
			console.log('[DeptDetail] deptCode : ', params.deptCode);
			dispatch(callDeptDetailAPI({	
				deptCode: params.deptCode
			}));  
		}
	,[]);

  /* 입력 핸들러 */
  const onChangeHandler = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  /* 수정 버튼 핸들러 */
  const onDeptModifyHandler = () => {
    if(!form.deptName || !form.deptSalary) {
      alert('값을 모두 입력해주세요.');
      return false;
    }
    console.log('[DeptDetail] onDeptModifyHandler');
    const formData = new FormData();
    formData.append("deptCode", dept.deptCode);
    formData.append("deptName", form.deptName);
    formData.append("deptSalary", form.deptSalary);
    dispatch(callDeptModifyAPI({
      form: formData
    }));
		alert('부서 수정 완료');
		navigate('/regist/dept/list', {replace:true});
  }

  /* 삭제 버튼 핸들러 */
  const onDeptDeleteHandler = () => {
    const answer = window.confirm( dept.deptName+ '(을/를) 정말 삭제하시겠습니까?');
    if(answer === true) {
      dispatch(callDeptDeleteAPI({	
        deptCode: params.deptCode
      })); 
    } else {
      return false;
    }    
		navigate('/regist/dept/list', { replace: true});
  }
  
  return (
    <div className="deptRegist">
      <h4>부서관리</h4>

      <table>
        <tbody>
          <tr>
            <td>부서코드</td>
            <td>
              <input
                type='number' 
                name='deptCode'
                value={dept.deptCode}
                onChange={ onChangeHandler }
                className='deptInput'
              />
            </td>
          </tr>
          <tr>
            <td>부서명</td>
            <td>
              <input 
                type='text'
                name='deptName'
                placeholder={dept.deptName}
                onChange={ onChangeHandler }
                className='deptInput'
              />
            </td>
          </tr>
          <tr>
            <td>부서수당</td>
            <td>
              <input 
                type='number'
                name='deptSalary'
                placeholder={dept.deptSalary}
                onChange={ onChangeHandler }
                className='deptInput'
              />
            </td>
          </tr>
        </tbody>
      </table>
      
      <div className='button3'>
        <button onClick={ onDeptModifyHandler }>수정</button>
        <button onClick={ onDeptDeleteHandler }>삭제</button>
        <button onClick={()=> navigate(-1)}>이전</button>
      </div>
    </div>
  );
}

export default DeptDetail;