import '../../../css/DeptList.css';
import '../../../css/Main.module.css';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { callDeptListAPI, callDeptsDeleteAPI } from '../../../apis/DeptAPICalls';
import searchImg from '../../../images/search.png'

function DeptList() {

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const depts = useSelector(state => state.deptReducer);
  const deptList = depts.data;
  const pageInfo = depts.pageInfo;
  const [currentPage, setCurrentPage] = useState(1);
  const [isDeleted, setIsDeleted] = useState(false);
  const [search, setSearch] = useState('');


  useEffect(
    () => {
      dispatch(callDeptListAPI({
        currentPage: currentPage
      }));
    }
    , [currentPage, isDeleted]
  );

  /* 페이징 */
  const pageNumber = [];
  if (pageInfo) {
    for (let i = 1; i <= pageInfo.pageEnd; i++) {
      pageNumber.push(i);
    }
  }

  /* 신규 버튼 핸들러 */
  const onRegistHandler = () => {
    navigate("/regist/dept/regist", { replace: false })
  }

  /* 체크 선택 핸들러 */
  const onSelectHandler = (e) => {
    const input = document.getElementsByTagName('input');
    console.log(input);
    for (var i = input.length - 1; i >= 0; i--) {
      if (e.target.checked === true) {
        input[i].checked = true;
      } else {
        input[i].checked = false;
      }
    }
  }

  /* 삭제 버튼 핸들러 */
  const onDeleteHandler = () => {
    const selectedDept = document.getElementsByTagName('input');
    const deptCodes = [];
    for (var i = selectedDept.length - 1; i >= 0; i--) {
      if (selectedDept[i].checked === true) {
        // console.log(selectedDept[i].parentElement.nextElementSibling.innerHTML)
        if (selectedDept[i].parentElement.nextElementSibling.innerHTML !== "부서코드") {
          const num = selectedDept[i].parentElement.nextElementSibling.firstChild.innerHTML;
          deptCodes.push(Number(num));
          console.log(deptCodes);
        }
      }
    }
    if (deptCodes.length > 0) {
      const answer = window.confirm(deptCodes + '번 부서를 정말 삭제하시겠습니까?');
      if (answer === true) {
        dispatch(callDeptsDeleteAPI({ deptCodes }));
        setIsDeleted(!isDeleted);
        window.location.reload();
      } else {
        return false;
      }
    } else {
      alert('삭제할 부서를 선택해주세요.');
    }
  }

  /* 부서코드 클릭 핸들러 */
  const onClickTableTr = (deptCode) => {
    navigate(`/regist/dept/detail/${deptCode}`, { replace: false });
  }

  /* 수정 버튼 핸들러 */
  const onModifyHandler = () => {

    const selectedDept = document.getElementsByTagName('input');

    const arr = []
    for (var i = selectedDept.length - 1; i >= 0; i--) {
      if (selectedDept[i].checked === true) {
        if (selectedDept[i].parentElement.nextElementSibling.innerHTML !== "부서코드") {
          arr.push(selectedDept[i].parentElement.nextElementSibling.firstChild.innerHTML)
          selectedDept[i].parentElement.nextElementSibling.style = 'color:red'
        }
      }
    }
    if (arr.length === 1) {
      navigate(`/regist/dept/detail/${arr[0]}`, { replace: false });
    } else if (arr.length <= 0) {
      alert('수정할 부서를 선택해주세요.')
    } else {
      alert('수정할 부서는 하나만 선택해주세요.')
    }
    console.log(arr);
  }

  /* 인쇄 버튼 핸들러 */
  const onPrintHandler = () => {
    // 2차 목표
  }



  /* 검색 기능 */
  const onSearchChangeHandler = (e) => {
    setSearch(e.target.value);
  }
  const onEnterkeyHandler = (e) => {
    if (e.key == 'Enter') {
      console.log('Enter key', search);
      navigate(`/regist/dept/search?value=${search}`, { replace: false });
      window.location.reload();
    }
  }
  const onSearchHandler = () => {
    navigate(`/regist/dept/search?value=${search}`, { replace: false });
    window.location.reload();
  }

  return (
    <div className="outlet">
      <h3>부서조회</h3>
      <div className='right'>
        <input
          type='text'
          placeholder="검색"
          value={search}
          onKeyUp={onEnterkeyHandler}
          onChange={onSearchChangeHandler}
        />
        <img onClick={onSearchHandler} src={searchImg} className='imgButton' />
      </div>
      <div className='pageButton'>
        {Array.isArray(deptList) &&
          <button
            className='activeButton'
            onClick={() => setCurrentPage(currentPage - 1)}
            disabled={currentPage === 1}
          >
            &lt;
          </button>
        }
        {pageNumber.map((num) => (
          <li key={num} onClick={() => setCurrentPage(num)}>
            <button
              className='inactiveButton'
              style={currentPage === num ? { backgroundColor: '#E8EBE7' } : null}
            >
              {num}
            </button>
          </li>
        ))}
        {Array.isArray(deptList) &&
          <button
            className='activeButton'
            onClick={() => setCurrentPage(currentPage + 1)}
            disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
          >
            &gt;
          </button>
        }
      </div>

      <table border={1} className='deptTable'>
        <thead>
          <tr>
            <th 
              style={{ width: 50 }}>
              <input 
                type='checkbox' 
                onChange={onSelectHandler} 
              />
            </th>
            <th>부서코드</th>
            <th>부서명</th>
            <th>부서수당</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(deptList) && deptList.map((dept) => (
            <tr
              key={dept.deptCode}
            >
              <td>
                <input 
                  type='checkbox' 
                />
              </td>
              <td 
                onClick={() => onClickTableTr(dept.deptCode)} 
                className='deptCode'>
                  <a href=''>{dept.deptCode}</a>
              </td>
              <td>{dept.deptName}</td>
              <td>{dept.deptSalary}</td>
            </tr>
          ))
          }
        </tbody>
      </table>

      <div className='button'>
        <button onClick={onRegistHandler}>신규</button>
        <button onClick={onPrintHandler}> 인쇄</button>
        <button onClick={onModifyHandler}>수정</button>
        <button onClick={onDeleteHandler}>삭제</button>
      </div>
    </div>

  );
}

export default DeptList;