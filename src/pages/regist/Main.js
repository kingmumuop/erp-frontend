
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { callStorageListAPI } from '../../apis/StorageAPICalls'
import { callProductListAPI } from '../../apis/ProductAPICalls'
import { callClientListAPI } from '../../apis/ClientAPICalls'
import { callPositionListAPI } from '../../apis/PositionAPICalls'
import { callDetpListAPI } from '../../apis/DeptAPICalls'
import { callEmpListAPI } from '../../apis/EmpAPICalls'

function RegistMain() {

	const dispatch = useDispatch();

	const storages = useSelector(state => state.storageReducer);
	const products = useSelector(state => state.productReducer);
	const clients = useSelector(state => state.clientReducer);
	const positions = useSelector(state => state.positionReducer);
	const depts = useSelector(state => state.deptReducer);
	const emps = useSelector(state => state.empReducer);

	const storageList = storages.data;
	const productList = products.data;
	const clientList = clients.data;
	const positionList = positions.data;
	const deptList = depts.data;
	const empList = emps.data;


	const pageInfo = storages.pageInfo;

	const [currentPage, setCurrentPage] = useState(1);

	const pageNumber = [];
	if(pageInfo){
		for(let i = 1; i <= pageInfo.pageEnd; i++){
			pageNumber.push(i);
		}
	}

	useEffect(
		() => {
			dispatch(callStorageListAPI({
				currentPage: currentPage
			}));
		}
		,[currentPage]
	);

	return (
		<>
		<div className='outlet' style={{float: 'left', width: '33%', padding: '10px'}}>
			<h4>창고조회</h4>
			<div style={{ listStyleType: "none", display: "flex", justifyContent: "left" }}>
				{ Array.isArray(storageList) &&
					<button
					className='activeButton'
					onClick={() => setCurrentPage(currentPage - 1)} 
					disabled={currentPage === 1}
				>
				&lt;
				</button>
				}

				{pageNumber.map((num) => (
					<li key={num} onClick={() => setCurrentPage(num)}>
						<button  
							className='inactiveButton' 
							style={ currentPage === num ? {backgroundColor : '#E8EBE7' } : null}
						>
							{num}
						</button>
				</li>
				))}
				{ Array.isArray(storageList) &&
					<button 
						className='activeButton'
						onClick={() => setCurrentPage(currentPage + 1)} 
						disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
					>
						&gt;
					</button>
					}
			</div>
			<table style={{borderColor: '#aaaaaa', borderSpacing: 0}}  border={1}>
				<thead style={{ backgroundColor: '#266666', color: '#ffffff'}}>
					<tr>
						<th >창고코드</th>
						<th >창고명</th>
						<th >창고유형</th>
					</tr>
				</thead>
				<tbody>
					{ Array.isArray(storageList) && storageList.map(( storage ) => (
						<tr key={ storage.storageCode }>
							<td>{ storage.storageCode }</td>
							<td>{ storage.storageName }</td>
							<td>{ storage.storageType }</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>

		<div className='outlet' style={{float: 'left', width: '33%', padding: '10px'}}>
			<h4>품목조회</h4>
			<div style={{ listStyleType: "none", display: "flex", justifyContent: "left" }}>
				{ Array.isArray(productList) &&
					<button
					className='activeButton'
					onClick={() => setCurrentPage(currentPage - 1)} 
					disabled={currentPage === 1}
				>
				&lt;
				</button>
				}

				{pageNumber.map((num) => (
					<li key={num} onClick={() => setCurrentPage(num)}>
						<button  
							className='inactiveButton' 
							style={ currentPage === num ? {backgroundColor : '#E8EBE7' } : null}
						>
							{num}
						</button>
				</li>
				))}
				{ Array.isArray(productList) &&
					<button 
						className='activeButton'
						onClick={() => setCurrentPage(currentPage + 1)} 
						disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
					>
						&gt;
					</button>
					}
			</div>
			<table style={{borderColor: '#aaaaaa', borderSpacing: 0}}  border={1}>
				<thead style={{ backgroundColor: '#266666', color: '#ffffff'}}>
					<tr>
						<th >품목코드</th>
						<th >품목명</th>
						<th >출고가</th>
					</tr>
				</thead>
				<tbody>
					{ Array.isArray(productList) && productList.map(( product ) => (
						<tr key={ product.productCode }>
							<td>{ product.productCode }</td>
							<td>{ product.productName }</td>
							<td>{ product.productFwdPriceA }</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>

		<div className='outlet' style={{float: 'left', width: '33%', padding: '10px'}}>
			<h4>거래처조회</h4>
			<div style={{ listStyleType: "none", display: "flex", justifyContent: "left" }}>
				{ Array.isArray(clientList) &&
					<button
					className='activeButton'
					onClick={() => setCurrentPage(currentPage - 1)} 
					disabled={currentPage === 1}
				>
				&lt;
				</button>
				}

				{pageNumber.map((num) => (
					<li key={num} onClick={() => setCurrentPage(num)}>
						<button  
							className='inactiveButton' 
							style={ currentPage === num ? {backgroundColor : '#E8EBE7' } : null}
						>
							{num}
						</button>
				</li>
				))}
				{ Array.isArray(clientList) &&
					<button 
						className='activeButton'
						onClick={() => setCurrentPage(currentPage + 1)} 
						disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
					>
						&gt;
					</button>
					}
			</div>
			<table style={{borderColor: '#aaaaaa', borderSpacing: 0}}  border={1}>
				<thead style={{ backgroundColor: '#266666', color: '#ffffff'}}>
					<tr>
						<th >거래처코드</th>
						<th >거래처명</th>
						<th >담당사원</th>
					</tr>
				</thead>
				<tbody>
					{ Array.isArray(clientList) && clientList.map(( client ) => (
						<tr key={ client.clientCode }>
							<td>{ client.clientCode }</td>
							<td>{ client.clientName }</td>
							<td>{ client.emp?.empName }</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>

		<div className='outlet' style={{float: 'left', width: '33%', padding: '10px'}}>
			<h4></h4>
			<div style={{ listStyleType: "none", display: "flex", justifyContent: "left" }}>
				{ Array.isArray(deptList) &&
					<button
					className='activeButton'
					onClick={() => setCurrentPage(currentPage - 1)} 
					disabled={currentPage === 1}
				>
				&lt;
				</button>
				}

				{pageNumber.map((num) => (
					<li key={num} onClick={() => setCurrentPage(num)}>
						<button  
							className='inactiveButton' 
							style={ currentPage === num ? {backgroundColor : '#E8EBE7' } : null}
						>
							{num}
						</button>
				</li>
				))}
				{ Array.isArray(deptList) &&
					<button 
						className='activeButton'
						onClick={() => setCurrentPage(currentPage + 1)} 
						disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
					>
						&gt;
					</button>
					}
			</div>
			<table style={{borderColor: '#aaaaaa', borderSpacing: 0}}  border={1}>
				<thead style={{ backgroundColor: '#266666', color: '#ffffff'}}>
					<tr>
						<th >q</th>
						<th >거래처명</th>
						<th >담당사원</th>
					</tr>
				</thead>
				<tbody>
					{ Array.isArray(deptList) && deptList.map(( dept ) => (
						<tr key={ dept.deptCode }>
							<td>{ dept.deptCode }</td>
							<td>{ dept.deptName }</td>
							<td>{ dept.emp?.empName }</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>




















		</>
	);
}

export default RegistMain;





