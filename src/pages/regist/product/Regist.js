import { useState, useRef, useEffect } from "react";
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import ProductRegistrationCSS from '../../../css/ProductRegist.css';

import { callProductRegistAPI } from '../../../apis/ProductAPICalls'

function ProductRegist() {

    const dispatch = useDispatch();

    const [image, setImage] = useState(null);
    const [imageUrl, setImageUrl] = useState();
    const imageInput = useRef();
    const navigate = useNavigate();


    const [form, setForm] = useState({
		productName: '',
        productRcvPrice: '',
        productFwdPriceA: '',
        productFwdPriceB: '',
        productFwdPriceC: '',
		productType: '',
        productImage: ''
	});

   
    useEffect(() => {

        /* 이미지 업로드시 미리보기 세팅 */
        if(image){
            const fileReader = new FileReader();
            fileReader.onload = (e) => {
                const { result } = e.target;
                if( result ){
                    setImageUrl(result);
                }
            }
            fileReader.readAsDataURL(image);
        }
    },
    [image]);
    

    
    const onChangeImageUpload = (e) => {

        const image = e.target.files[0];

        setImage(image);
    };

    const onClickImageUpload = () => {
        imageInput.current.click();
    }

    const onChangeHandler = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    };
    
    const onClickProductRegistHandler = () => {
		
		const formData = new FormData();

		formData.append("productName", form.productName);
        formData.append("productRcvPrice", form.productRcvPrice);
        formData.append("productFwdPriceA", form.productFwdPriceA);
        formData.append("productFwdPriceB", form.productFwdPriceB);
        formData.append("productFwdPriceC", form.productFwdPriceC);
		formData.append("productType", form.productType);

        console.log(image);

        if(image){
            formData.append("productImage", image);
        }

        console.log(form);

		dispatch(callProductRegistAPI({
			form: formData
		}));

        console.log("form : " + form);
		alert('품목 등록 완료');
		navigate('/regist/product/list', {replace:true});
		window.location.reload();
	}
    
    return (
        <>
          <div>
            <h4>품목 등록</h4>
            <table>
                <tbody>
                <tr>
                    <td>품목명</td>
                    <td>
                        <input 
                            type="text"
                            name="productName"
                            onChange={onChangeHandler}
                        />
                    </td>
                </tr>

                <tr>
                    <td>입고단가</td>
                    <td>
                        <input 
                            type="text"
                            name="productRcvPrice"
                            onChange={onChangeHandler}
                        />
                    </td>
                </tr>

                <tr>
                    <td>출고단가A</td>
                    <td>
                        <input 
                            type="text"
                            name="productFwdPriceA"
                            onChange={onChangeHandler}
                        />
                    </td>
                </tr>

                <tr>
                    <td>출고단가B</td>
                    <td>
                        <input 
                            type="text"
                            name="productFwdPriceB"
                            onChange={onChangeHandler}
                        />
                    </td>
                </tr>

                <tr>
                    <td>출고단가C</td>
                    <td>
                        <input 
                            type="text"
                            name="productFwdPriceC"
                            onChange={onChangeHandler}
                        />
                    </td>
                </tr>

                <tr>
                    <td>구분</td>
                    <td>
                        <input 
                            type="text"
                            name="productType"
                            onChange={onChangeHandler}
                        />
                    </td>
                </tr>
                </tbody>
            </table>

            <div className={ ProductRegistrationCSS.productSection }>
                <div className={ ProductRegistrationCSS.productInfoDiv }>
                    <div className={ ProductRegistrationCSS.productImageDiv }>
                        { imageUrl && <img 
                            className={ ProductRegistrationCSS.productImage } 
                            src={ imageUrl } 
                            alt="preview"
                        />}
                        <input                
                            style={ { display: 'none' }}
                            type="file"
                            name='productImage' 
                            accept='image/jpg,image/png,image/jpeg,image/gif'
                            onChange={ onChangeImageUpload }
                            ref={ imageInput }
                        />
                        <button 
                            className={ ProductRegistrationCSS.productImageButton }
                            onClick={ onClickImageUpload } 
                        >
                            이미지 업로드
                            </button>
                    </div>
                </div>
            </div>

            <div>
                <button onClick={onClickProductRegistHandler}>등록</button>
            </div>

          </div>
        </>
    )

}

export default ProductRegist;