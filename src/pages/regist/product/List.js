import { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import '../../../css/ProductList.css';

import { callProductListAPI, callProductDeleteAPI } from '../../../apis/ProductAPICalls';

function ProductList() {

    const dispatch = useDispatch();
	const products = useSelector(state => state.productReducer);
	const productList = products.data;
	const navigate = useNavigate();
	const pageInfo = products.pageInfo;

    const [currentPage, setCurrentPage] = useState(1);
	const [search, setSearch] = useState('');

    const pageNumber = [];
	if(pageInfo){
		for(let i = 1; i <= pageInfo.pageEnd; i++){
			pageNumber.push(i);
		}
	}

    useEffect(
		() => {
			dispatch(callProductListAPI({
				currentPage: currentPage
			}));
		}
		,[currentPage]
	);

	const onClickProductRegist = () => {
		navigate("/regist/product/regist", { replace: false })

	}

	const onClickProductDetail = (productCode) => {
		navigate(`/regist/product/detail/${productCode}`, { replace: false });
	}

	const onSearchChangeHandler = (e) => {
		setSearch(e.target.value);
	}

	const onEnterkeyHandler = (e) => {
		if (e.key == 'Enter') {
				console.log('Enter key', search);
				
				navigate(`/regist/product/search?value=${search}`, { replace: false });
				
				window.location.reload();
		}
  }


    return (
            <>
            <div className="outlet">
                    <h1>품목 조회</h1>
                    <hr/>

				<input 
                    type="text" 
                    placeholder="품목명 검색" 
                    value={ search }
                    onKeyUp={ onEnterkeyHandler }
                    onChange={ onSearchChangeHandler }
                />	

                    <div style={{ listStyleType: "none", display: "flex", justifyContent: "left" }}>
				{ Array.isArray(productList) &&
					<button
					
					onClick={() => setCurrentPage(currentPage - 1)} 
					disabled={currentPage === 1}
				>
				&lt;
				</button>
				}

				{pageNumber.map((num) => (
					<li key={num} onClick={() => setCurrentPage(num)}>
						<button  
							
							style={ currentPage === num ? {backgroundColor : '#E8EBE7' } : null}
						>
							{num}
						</button>
				</li>
				))}
				{ Array.isArray(productList) &&
					<button 
					
						onClick={() => setCurrentPage(currentPage + 1)} 
						disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
					>
						&gt;
					</button>
					}
			</div>

            <table border={1} className='productTable'>
				<thead>
					<tr>
						<th >품목코드</th>
						<th >품목명</th>
                        <th >입고단가</th>
                        <th >A단가</th>
                        <th >B단가</th>
                        <th >C단가</th>
                        <th >구분</th>
						<th >품목사진</th>
						{/* <th >품목사진</th> */}
					</tr>
				</thead>
				<tbody>
					{ Array.isArray(productList) && productList.map(( product ) => (
						<tr key={ product.productCode }
							onClick={ () => onClickProductDetail(product.productCode)}
						>
							<td>{ product.productCode }</td>
                            <td>{ product.productName }</td>
							<td>{ product.productRcvPrice }</td>
                            <td>{ product.productFwdPriceA }</td>
                            <td>{ product.productFwdPriceB }</td>
                            <td>{ product.productFwdPriceC }</td>
                            <td>{ product.productType }</td>
							<td><img width="50px" src={ product.productImageUrl} alt="이미지 없음" /></td>
							{/* <td>{ product.productImageUrl }</td> */}
						</tr>
					))}
				</tbody>
			</table>
			
			
				<button onClick={onClickProductRegist}>등록</button>
				<button>삭제</button>
			
		</div>


           
              

                
                
            </>
    );
}

export default ProductList;