import '../../../css/Main.module.css';

import { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { callEmpListAPI, callEmployeesDeleteAPI } from '../../../apis/EmpAPICalls';


function EmpList() {

	const dispatch = useDispatch();
	const employees = useSelector(state => state.empReducer);
	const employeeList = employees.data;

	const navigate = useNavigate();
	const pageInfo = employees.pageInfo;

	const [currentPage, setCurrentPage] = useState(1);
	const [search, setSearch] = useState('');
	const [isTrue, setIsTrue] = useState(false);

	const [checkedList, setCheckedList] = useState([]);
	// 1️⃣ onChange함수를 사용하여 이벤트 감지, 필요한 값 받아오기
	const onCheckedElement = (checked, employee) => {
	  if (checked) {
		setCheckedList([...checkedList, employee.empCode]);
	  } else if (!checked) {
		setCheckedList(checkedList.filter(el => el !== employee.empCode));
	  }
	};

	const pageNumber = [];
	if(pageInfo){
		for(let i = 1; i <= pageInfo.pageEnd; i++){
			pageNumber.push(i);
		}
	}

	const onSelectHandler = (e) => {
    setIsTrue(false);
    const input = document.getElementsByTagName('input');
    // console.log(input.length);
    for(var i = input.length-1; i >= 0; i--) {
      if(input[0].checked === true) {
        input[i].checked = true;
      } else {
        input[i].checked = false;
      }
    }
  }

/* 삭제 핸들러 */
const onClickDeleteHandler = () => {

	const employeeCodes = [] = [...checkedList]

	console.log(employeeCodes);
	
	console.log(employeeCodes);
	dispatch(callEmployeesDeleteAPI({
		empCodes: JSON.stringify(employeeCodes)
	}));
		alert('사원 복수 삭제 완료');
		navigate('/regist/emp/list', { reload: true });
		window.location.reload();
	}

	useEffect(
		() => {
			dispatch(callEmpListAPI({
				currentPage: currentPage
			}));
		}
		,[currentPage]
	);

	const onClickEmployeeRegist = () => {
		navigate('/regist/emp/regist', { replace: false })
	}

	const onClickTableTr = (employee) => {
		navigate(`/regist/emp/update`, { replace : false, state: employee });
	}

	const onSearchChangeHandler = (e) => {
		setSearch(e.target.value);
	}

	const onEnterkeyHandler = (e) => {
		if (e.key == 'Enter') {
				console.log('Enter key', search);
				
				navigate(`/regist/employee/search?value=${search}`, { replace: false });
				
				window.location.reload();
		}
  }

	return (
		<>
		<div className='outlet'>
			<h4>사원 조회</h4>
			<div>
				<input
				type="text"
				value = { search }
				onKeyUp = { onEnterkeyHandler }
				onChange={ onSearchChangeHandler }
				/>

			</div>


			<div style={{ listStyleType: "none", display: "flex", justifyContent: "left" }}>
				{ Array.isArray(employeeList) &&
					<button
					className='activeButton'
					onClick={() => setCurrentPage(currentPage - 1)} 
					disabled={currentPage === 1}
				>
				&lt;
				</button>
				}

				{pageNumber.map((num) => (
					<li key={num} onClick={() => setCurrentPage(num)}>
						<button  
							className='inactiveButton' 
							style={ currentPage === num ? {backgroundColor : '#E8EBE7' } : null}
						>
							{num}
						</button>
				</li>
				))}
				{ Array.isArray(employeeList) &&
					<button 
						className='activeButton'
						onClick={() => setCurrentPage(currentPage + 1)} 
						disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
					>
						&gt;
					</button>
					}
			</div>

			<table style={{borderColor: '#aaaaaa', borderSpacing: 0}}  border={1}>
				<thead style={{ backgroundColor: '#266666', color: '#ffffff'}}>
					<tr>
						<th style= {{width: 10}}><input type='checkbox' onChange={onSelectHandler}/></th>
						<th >입사일자</th>
						<th >사원번호</th>
						<th >성명</th>
						<th >부서명</th>
						<th >직급</th>
						<th >Email</th>
						<th >계좌번호</th>
						<th >급여구분</th>
					</tr>
				</thead>
				<tbody>
					{ Array.isArray(employeeList) && employeeList.map(( employee ) => (
						<tr key={ employee.employeeCode }>
							<td><input type='checkbox' onChange={e => onCheckedElement(e.target.checked, employee)} /></td>
							<td onClick = { () => onClickTableTr( employee )}>{ employee.empEntdate }</td>
							<td onClick = { () => onClickTableTr( employee )}>{ employee.empCode }</td>
							<td onClick = { () => onClickTableTr( employee )}>{ employee.empName }</td>
							<td onClick = { () => onClickTableTr( employee )}>{ employee.dept.deptName }</td>
							<td onClick = { () => onClickTableTr( employee )}>{ employee.position.positionName }</td>
							<td onClick = { () => onClickTableTr( employee )}>{ employee.empEmail }</td>
							<td onClick = { () => onClickTableTr( employee )}>{ employee.empAccount }</td>
							<td onClick = { () => onClickTableTr( employee )}>{ "고정급" }</td>

						
						</tr>
					))}
				</tbody>
			</table>
			<div>
				<button className='mainButton' onClick={ onClickEmployeeRegist }>신규</button>
				<button className='subButton' onClick={ onClickDeleteHandler }>삭제</button>
			</div>
		</div>
		
		</>
	);
}

export default EmpList;





