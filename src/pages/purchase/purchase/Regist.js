import '../../../css/Main.module.css';

import Modal from '../../../components/modal/Modal.js';

import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useState, useRef } from "react";

import ClientList from '../../../components/regist/ClientSelectList'
import EmpList from '../../../components/regist/EmpSelectList'
import StockList from '../../../components/stock/StockSelectList'

import { callPurchaseRegistAPI } from '../../../apis/PurchaseAPICalls'

function PurchaseRegist() {

	const inputRef = useRef({});
  
	const dispatch = useDispatch();
	const navigate = useNavigate();

		/* 등록 핸들러 */
		const onClickPurchaseRegistHandler = () => {
			console.log('[PurchaseRegist] onClickPurchaseRegistHandler');
			const registForm = {
				
				purchaseDate: form.purchaseDate,
					client: {
						clientCode: form.clientCode
					},
					emp: {
						empCode: form.empCode
					},
					purchaseDetail: form.purchaseDetails.map(detail => ({	// purchaseDetail자체에 map을 걸어서 배열로 값을 받도록 한다
						purchaseAmount: detail.purchaseAmount,
						purchaseNote: detail.purchaseNote,
						stock: {
							stockCode: detail.stockCode
						}
					}))
			}
			dispatch(callPurchaseRegistAPI({ form: registForm }));
			alert('주문 등록 완료');
	}

	const [form, setForm] = useState({
		purchaseDate: '',
		clientCode: 0,
		empCode: 0,
		purchaseDetails: [],		// 빈 배열을 생성
	});

		// detail 추가 버튼
		const addPurchaseDetail = (purchaseAmount = '', purchaseNote = '', stockCode = 0) => {
			setForm(prevState => {
				const purchaseDetails = [...prevState.purchaseDetails];
				if(!purchaseDetails.find(detail => detail.stockCode === stockCode)){	//  find: purchaseDetail에서 값 찾는 명령어(중복값 방지)
					purchaseDetails.unshift({purchaseAmount, purchaseNote, stockCode});		// unshift: 입력된 값이 자동으로 위로 올라가게 함
				}
				return {...prevState, purchaseDetails};
			});
		}
	
		// detail 삭제 버튼
		const removePurchaseDetail = (index) => {
			setForm({
					...form,
					purchaseDetails: form.purchaseDetails.filter((_, i) => i !== index),
			});
	}
 
	const [clientModalOpen, setClientModalOpen] = useState(false)
	const [empModalOpen, setEmpModalOpen] = useState(false)
	const [stockModalOpen, setStockModalOpen] = useState(false)

	const selectClient = (chosenClientCode, chosenClientName) => {
    setForm({...form, clientCode : chosenClientCode, clientName : chosenClientName});	// ...form을 통해 setForm에 기존 입력된 정보가 누적되도록 한다 !important
    console.log("넘겨받은 고객코드: " + form.clientCode)
	}

	const selectEmp = (chosenEmpCode, chosenEmpName) => {
    setForm({...form, empCode : chosenEmpCode, empName : chosenEmpName});
    console.log("넘겨받은 사원코드: " + form.empCode)
	}

	const selectStock = (chosenStockCode, chosenStorageCode, chosenStorageName, chosenProductCode, chosenProductName, chosenProductFwdPriceA) => {
    setForm(prevForm => ({		// 배열의 이전 단계 form을 받아오기 위한 구문
      ...prevForm,
      purchaseDetails: [
        ...prevForm.purchaseDetails,
        {
         	stockCode: chosenStockCode,
					storageCode: chosenStorageCode,
					storageName: chosenStorageName,
					productCode: chosenProductCode,
          productName: chosenProductName,
          productFwdPriceA: chosenProductFwdPriceA,
        }
      ]
    }));
    console.log("넘겨받은 재고명: " + chosenProductName)
  }

	/* 모달 여닫이 */
	const openClientModal = () => {
		setClientModalOpen(true, { replace: false });
	};
	
	const closeClientModal = () => {
		setClientModalOpen(false);
				console.log("고객 코드 출력 : " + form.clientCode);
	};

	const openEmpModal = () => {
		setEmpModalOpen(true, { replace: false });
	};
	
	const closeEmpModal = () => {
		setEmpModalOpen(false);
				console.log("사원 코드 출력 : " + form.empCode);
	};

	const openStockModal = () => {
		setStockModalOpen(true, { replace: false });
	};
	
	const closeStockModal = () => {
		setStockModalOpen(false);
				console.log("재고 코드 출력 : " + form.stockCode);
	};

	const onChangeHandler = (e) => {
		setForm({
			...form, 
			[e.target.name]: e.target.value
		});
	};

	const handleChange = (e, index, key) => {
    const value = e.target.value;
    setForm(prevState => {
			const purchaseDetails = [...prevState.purchaseDetails];
			purchaseDetails[index][key] = value;
			purchaseDetails[index]["purchasePrice"] = purchaseDetails[index]["productFwdPriceA"] * purchaseDetails[index]["purchaseAmount"]
			return { ...prevState, purchaseDetails };
    });
	}

	return (
		<>
			<Modal open={clientModalOpen} close={closeClientModal} header=' 거래처 선택' >
				<ClientList selectClient={selectClient} close={closeClientModal} />
			</Modal>
			<Modal open={empModalOpen} close={closeEmpModal} header=' 사원 선택' >
				<EmpList selectEmp={selectEmp} close={closeEmpModal} />
			</Modal>
			<Modal open={stockModalOpen} close={closeStockModal} header=' 재고 선택' >
				<StockList selectStock={selectStock} close={closeStockModal} />
			</Modal>

			<div>
				<h5>판매등록<hr/></h5>
				<table border={1}>
					<tbody>
						<tr>
							<th>거래처명</th>
							<td>
								<input type="number" name="clientCode" onChange={onChangeHandler} onClick={openClientModal} value={form.clientCode} ref={inputRef}/>
								<button onClick={openClientModal}>조회</button>
							</td>

							<th>담당자명</th>
							<td>
								<input type="number" name="empCode" onChange={onChangeHandler} onClick={openEmpModal} value={form.empCode} ref={inputRef}/>
								<button  onClick={openEmpModal}>조회</button>
							</td>
						</tr>
						
						<tr>
							<th>주문상태</th>
							<td>
								<label>미확인</label>
							</td>
						</tr>

						<tr>
							<th>납기일자</th>
							<td>
								<input type="date" name="purchaseDelivery" onChange={onChangeHandler} value={form.purchaseDelivery} ref={inputRef}/>
							</td>
						</tr>
					</tbody>
				</table>
				<button onClick={() => addPurchaseDetail()}> + </button>		
				<table border={1}>
				
					<thead>
						<tr>
							<th>재고코드</th>
							<th>창고코드</th>
							<th>창고명</th>
							<th>품목코드</th>
							<th>품목명</th>
							<th>주문단가</th>
							<th>주문수량</th>
							<th>금액합계</th>
							<th>비고</th>
							<th>삭제</th>
						</tr>
					</thead>
					<tbody>
						{form.purchaseDetails.map((detail, index) => (
							<tr key={index}> 
								<td onClick={openStockModal} value={form.stockCode}>{detail.stockCode}</td>
								<td onClick={openStockModal} value={form.storageCode}>{detail.storageCode}</td>
								<td onClick={openStockModal} value={form.storageName}>{detail.storageName}</td>
								<td onClick={openStockModal} value={form.productCode}>{detail.productCode}</td>
								<td onClick={openStockModal} value={form.productame}>{detail.productName}</td>
								<td>{detail.productFwdPriceA}</td>
								<td><input type="number" value={detail.purchaseAmount} onChange={(e) => handleChange(e, index, 'purchaseAmount')}/></td>
								<td>{!isNaN(detail.productFwdPriceA * detail.purchaseAmount) ? (detail.productFwdPriceA * detail.purchaseAmount) : null}</td>
								<td><input type="text" value={detail.purchaseNote} onChange={(e) => handleChange(e, index, 'purchaseNote')}/></td>
								<td>
									<button onClick={() => removePurchaseDetail(index)}> - </button>	
								</td>
							</tr>
						))}
					</tbody>
					
				</table>

			</div>
			<div>
				<button	className='mainButton' onClick={ onClickPurchaseRegistHandler }>	등록 </button>
				<button className='subButton' onClick={ () => navigate(-1) }> 이전 </button>
			</div>
		</>
	)
}

export default PurchaseRegist;