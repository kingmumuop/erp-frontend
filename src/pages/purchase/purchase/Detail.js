import '../../../css/Main.module.css';

import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import Modal from '../../../components/modal/Modal.js';

import EmpList from '../../../components/regist/EmpSelectList'
import ClientList from '../../../components/regist/ClientSelectList'
import StockList from '../../../components/stock/StockSelectList'

import {
	callPurchaseDetailAPI,
	callPurchaseUpdateAPI
} from '../../../apis/PurchaseAPICalls';

function PurchaseDetail() {

	const dispatch = useDispatch();
	const params = useParams();
	const purchase  = useSelector(state => state.purchaseReducer);

	const [modifyMode, setModifyMode] = useState(false);
	const navigate = useNavigate();

	const [form, setForm] = useState({
		purchaseDetails: []
	});

	useEffect(        
		() => {
			console.log('[PurchaseDetail] purchaseCode : ', params.purchaseCode);
			dispatch(callPurchaseDetailAPI({	
				purchaseCode: params.purchaseCode
			}));
		}
	,[]);

	/* 수정 모드 핸들러 */
	const onClickModifyModeHandler = () => {
		console.log("확인" + purchase.purchaseDetail?.stock?.product?.productName);
		setModifyMode(true);
		setForm({
			purchaseCode: purchase.purchaseCode,
			purchaseDate: purchase.purchaseDate,
			clientCode: purchase?.client?.clientCode,
			empCode: purchase.emp?.empCode,
			purchaseDetails: [{
				purchaseAmount: purchase.purchaseDetail?.purchaseAmount,
				purchaseNote: purchase.purchaseDetail?.purchaseNote,
				stockCode: purchase.purchaseDetail?.stock?.stockCode,
				// storageCode: sales.salesDetail?.stock?.storage?.storaegCode,
				// storageName: sales.salesDetail?.stock?.storage?.storageName,
				// productCode: sales.salesDetail?.stock?.product?.productCode,
				// productName: sales.salesDetail?.stock?.product?.productName,
				// productFwdPriceA: sales.salesDetail?.stock?.product?.productFwdPriceA
			}]
		});
	}

	/* form 데이터 세팅 */  
	const onChangeHandler = (e) => {
		setForm({
			...form,
			[e.target.name]: e.target.value
		});
	};

	/* 수정 핸들러 */
	const onClickPurchaseUpdateHandler = () => {
    console.log('[PurchaseUpdate] onClickPurchaseUpdateHandler');
    const updateForm = {
		purchaseCode : form.purchaseCode,
		purchaseDate : form.purchaseDate,
			client : {
				clientCode : form.clientCode
			},
			emp : {
				empCode : form.empCode
			},
			storage : {
				storageCode : form.storageCode
			},
			purchaseDetail : form.purchaseDetails.map(detail => ({
				purchaseAmount : detail.purchaseAmount,
				purchaseNote : detail.purchaseNote,
				stock: {
							stockCode: detail.stockCode
				}
			}))
    }

	dispatch(callPurchaseUpdateAPI({
		form: updateForm }));         
	alert('주문 수정 완료');
	navigate('/purchase/purchase/list', { replace: true});
	window.location.reload();
}

		// detail 추가 버튼
		const addPurchaseDetail = (purchaseAmount = '', purchaseNote = '', stockCode = '') => {
			setForm(prevState => {
				const purchaseDetails = [...prevState.purchaseDetails];
				if(!purchaseDetails.find(detail => detail.stockCode === stockCode)){	
					purchaseDetails.unshift({purchaseAmount, purchaseNote, stockCode});	
				}
				return {...prevState, purchaseDetails};
			});
		}
		
		// detail 삭제 버튼
		const removePurchaseDetail = (index) => {
			setForm({
					...form,
					purchaseDetails: form.purchaseDetails.filter((_, i) => i !== index),
			});
		}

	const [clientModalOpen, setClientModalOpen] = useState(false)
	const [empModalOpen, setEmpModalOpen] = useState(false)
	const [stockModalOpen, setStockModalOpen] = useState(false)

	const selectClient = (chosenClientCode, chosenClientName) => {
    setForm({...form, clientCode : chosenClientCode, clientName : chosenClientName});
    console.log("넘겨받은 고객코드: " + form.clientCode)
	}

	const selectEmp = (chosenEmpCode, chosenEmpName) => {
    setForm({...form, empCode : chosenEmpCode, empName : chosenEmpName});
    console.log("넘겨받은 사원코드: " + form.empCode)
	}

	const selectStock = (chosenStockCode, chosenStorageCode, chosenStorageName, chosenProductCode, chosenProductName, chosenProductFwdPriceA) => {
    setForm(prevForm => ({
      ...prevForm,
      purchaseDetails: [
        ...prevForm.purchaseDetails,
        {
					stockCode: chosenStockCode,
					storageCode: chosenStorageCode,
					storageName: chosenStorageName,
          productCode: chosenProductCode,
          productName: chosenProductName,
          productFwdPriceA: chosenProductFwdPriceA,
        }
      ]
    }));
    console.log("넘겨받은 품목코드: " + form.purchaseDetails.length)
  }

	/* 모달 여닫이 */
	const openClientModal = () => {
		setClientModalOpen(true, { replace: false });
	};
	
	const closeClientModal = () => {
		setClientModalOpen(false);
				console.log("고객 코드 출력 : " + form.clientCode );
	};

	const openEmpModal = () => {
		setEmpModalOpen(true, { replace: false });
	};
	
	const closeEmpModal = () => {
		setEmpModalOpen(false);
				console.log("사원 코드 출력 : " + form.empCode );
	};

	const openStockModal = () => {
		setStockModalOpen(true, { replace: false });
	};
	
	const closeStockModal = () => {
		setStockModalOpen(false);
				console.log("재고 코드 출력 : " + form.stockCode );
	};

	const handleChange = (e, index, key) => {
    const value = e.target.value;
    setForm(prevState => {
			const purchaseDetails = [...prevState.purchaseDetails];
			purchaseDetails[index][key] = value;
			purchaseDetails[index]["purchasePrice"] = purchaseDetails[index]["productFwdPriceA"] * purchaseDetails[index]["purchaseAmount"]
			return { ...prevState, purchaseDetails };
    });	
	}

  return (
		<>
		<Modal open={clientModalOpen} close={closeClientModal} header=' 거래처 선택' >
			<ClientList selectClient={selectClient} close={closeClientModal} />
		</Modal>
		<Modal open={empModalOpen} close={closeEmpModal} header=' 사원 선택' >
			<EmpList selectEmp={selectEmp} close={closeEmpModal} />
		</Modal>
		<Modal open={stockModalOpen} close={closeStockModal} header=' 재고 선택' >
			<StockList selectStock={selectStock} close={closeStockModal} />
		</Modal>
		
		<div>
			<h4>판매수정</h4>
			{ purchase &&
			<table border={1}>
				<tbody>
					<tr>
						<td>판매코드</td>
						<td>{ purchase.purchaseCode }</td>
					</tr>
					<tr>
						<td>판매일자</td>
						<td>{ purchase.purchaseDate }</td>
					</tr>
					<tr>
						<td>거래처명</td>
						<td>
							<input
								type="text" name="clientCode" onChange={ onChangeHandler } onClick={ (openClientModal ) }
								value={ (modifyMode ?  form.clientCode : purchase.client?.clientCode) || ''}
								readOnly={ modifyMode ? false : true }
								style={ modifyMode ? null : { border: 'none', background: 'transparent'}}
							/>
							<label readOnly={ modifyMode ? false : true } >{ (modifyMode ?  form.clientName : purchase.client?.clientName) || ''}</label>
						</td>
					</tr>
					<tr>
						<td>담당자명</td>
						<td>
							<input
								type="text" name="empCode" onChange={ onChangeHandler } onClick={ (openEmpModal ) }
								value={ (modifyMode ?  form.empCode : purchase.emp?.empCode) || ''}
								readOnly={ modifyMode ? false : true }
								style={ modifyMode ? null : { border: 'none', background: 'transparent'}}
							/>
							<label readOnly={ modifyMode ? false : true } >{ (modifyMode ?  form.empName : purchase.emp?.dept?.deptName+' '+purchase.emp?.empName+' '+purchase.emp?.position?.positionName) || ''}</label>
						</td>
					</tr>
				</tbody>
			</table>
			}
			<hr/>

			{purchase &&
			<table border={1}>
			<thead>
				<tr>
					<th>재고코드</th>
					<th>창고코드</th>
					<th>창고명</th>
					<th>품목코드</th>
					<th>품목명</th>
					<th>판매단가</th>
					<th>판매수량</th>
					<th>금액합계</th>
					<th>비고</th>
					<th><button onClick = {addPurchaseDetail}> + </button></th>
				</tr>
			</thead>
			<tbody>
				{modifyMode ? (
					form.purchaseDetails.map((detail, index) => (

						<tr key={index}>
							<td onClick={openStockModal} value={form.stockCode}>{detail.stockCode}</td>
							<td onClick={openStockModal} >{detail.storageCode}</td>
							<td onClick={openStockModal} >{detail.storageName}</td>
							<td onClick={openStockModal} >{detail.productCode}</td>
							<td onClick={openStockModal} >{detail.productName}</td>
							<td>{detail.productFwdPriceA}</td>
							<td><input type="number" value={detail.purchaseAmount} onChange={(e) => handleChange(e, index, 'purchaseAmount')}/></td>
							<td>{!isNaN(detail?.productFwdPriceA * detail?.purchaseAmount) ? (detail?.productFwdPriceA * detail?.purchaseAmount) : ''}</td>
							<td><input type="text" value={detail.purchaseNote} onChange={(e) => handleChange(e, index, 'purchaseNote')}/></td>
							<td>
								<button onClick={() => removePurchaseDetail(index)}> - </button>	
							</td>
						</tr>
					))
				) : (
					purchase?.purchaseDetail?.map((detail, index) => (
						<tr key={index}>
							<td>{detail?.stock?.stockCode}</td>
							<td>{detail?.stock?.storage?.storageCode}</td>
							<td>{detail?.stock?.storage?.storageName}</td>
							<td>{detail?.stock?.product?.productCode}</td>
							<td>{detail?.stock?.product?.productName}</td>
							<td>{detail?.stock?.product?.productFwdPriceA}</td>
							<td>{detail?.purchaseAmount}</td>
							<td>{!isNaN(detail?.stock?.product?.productFwdPriceA*detail?.purchaseAmount) ? (detail?.stock?.product?.productFwdPriceA*detail?.purchaseAmount) : ''}</td>
							<td>{detail?.purchaseNote}</td>
							<td></td>
						</tr>
					))
				)}
			</tbody>
		</table>
			}
		</div>
			<div>
				{!modifyMode && <button className='mainButton' onClick={ onClickModifyModeHandler } > 수정 </button> }
				{ modifyMode && <button className='mainButton' onClick={ onClickPurchaseUpdateHandler } > 저장 </button>	}
				<button className='subButton' onClick={ () => navigate(-1) }> 이전 </button>
			</div>
		</>
	);
}

export default PurchaseDetail;













