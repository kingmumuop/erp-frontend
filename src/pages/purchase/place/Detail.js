import '../../../css/Main.module.css';

import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import Modal from '../../../components/modal/Modal.js';

import EmpList from '../../../components/regist/EmpSelectList'
import ClientList from '../../../components/regist/ClientSelectList'
import StorageList from '../../../components/regist/StorageSelectList'
import ProductList from '../../../components/regist/ProductSelectList'

import {
	callPlaceDetailAPI,
	callPlaceUpdateAPI
} from '../../../apis/PlaceAPICalls';

function PlaceDetail() {

	const dispatch = useDispatch();
	const params = useParams();
	const place  = useSelector(state => state.placeReducer);

	const [modifyMode, setModifyMode] = useState(false);
	const navigate = useNavigate();

	const [form, setForm] = useState({
		placeDetails: []
	});

	useEffect(        
		() => {
			console.log('[PlaceDetail] placeCode : ', params.placeCode);
			dispatch(callPlaceDetailAPI({	
				placeCode: params.placeCode
			}));
		}
	,[]);

	/* 수정 모드 핸들러 */
	const onClickModifyModeHandler = () => {
		console.log("확인" + place.placeDetail.product?.productCode);
		setModifyMode(true);
		setForm({
			placeCode: place.placeCode,
			placeDate: place.placeDate,
			placeDelivery: place.Delivery,
			placeStatus: place.placeStatus,
			clientCode: place?.client?.clientCode,
			empCode: place.emp?.empCode,
			storageCode: place.storage?.storageCode,
			placeDetails: [{
				placeAmount: place.placeDetail?.placeAmount,
				placeNote: place.placeDetail?.placeNote,
				productCode: place.placeDetail?.product?.productCode
			}]
		});
	}

	/* form 데이터 세팅 */  
	const onChangeHandler = (e) => {
		setForm({
			...form,
			[e.target.name]: e.target.value
		});
	};

	/* 수정 핸들러 */
	const onClickPlaceUpdateHandler = () => {
    console.log('[PlaceUpdate] onClickPlaceUpdateHandler');
    const updateForm = {
			placeCode : form.placeCode,
			placeDelivery : form.placeDelivery,
			placeStatus : form.placeStatus,
			client : {
				clientCode : form.clientCode
			},
			emp : {
				empCode : form.empCode
			},
			storage : {
				storageCode : form.storageCode
			},
			placeDetail : form.placeDetails.map(detail => ({
				placeAmount : detail.placeAmount,
				placeNote : detail.placeNote,
				product: {
							productCode: detail.productCode
				}
			}))
    }

	dispatch(callPlaceUpdateAPI({
		form: updateForm }));         
	alert('발주 수정 완료');
	navigate('/purchase/place/list', { replace: true});
	window.location.reload();

}

		// detail 추가 버튼
		const addPlaceDetail = (placeAmount = '', placeNote = '', productCode = '') => {
			setForm(prevState => {
				const placeDetails = [...prevState.placeDetails];
				if(!placeDetails.find(detail => detail.productCode === productCode)){	
					placeDetails.unshift({placeAmount, placeNote, productCode});	
				}
				return {...prevState, placeDetails};
			});
		}
		
		// detail 삭제 버튼
		const removePlaceDetail = (index) => {
			setForm({
					...form,
					placeDetails: form.placeDetails.filter((_, i) => i !== index),
			});
		}

	const [clientModalOpen, setClientModalOpen] = useState(false)
	const [empModalOpen, setEmpModalOpen] = useState(false)
	const [storageModalOpen, setStorageModalOpen] = useState(false)
	const [productModalOpen, setProductModalOpen] = useState(false)

	const selectClient = (chosenClientCode, chosenClientName) => {
    setForm({...form, clientCode : chosenClientCode, clientName : chosenClientName});
    console.log("넘겨받은 고객코드: " + form.clientCode)
	}

	const selectEmp = (chosenEmpCode, chosenEmpName) => {
    setForm({...form, empCode : chosenEmpCode, empName : chosenEmpName});
    console.log("넘겨받은 사원코드: " + form.empCode)
	}

	const selectStorage = (chosenStorageCode, chosenStorageName) => {
    setForm({...form, storageCode : chosenStorageCode, storageName : chosenStorageName});
    console.log("넘겨받은 창고코드: " + form.storageCode)
	}

	const selectProduct = (chosenProductCode, chosenProductName, chosenProductFwdPriceA) => {
    setForm(prevForm => ({
      ...prevForm,
      placeDetails: [
        ...prevForm.placeDetails,
        {
          productCode: chosenProductCode,
          productName: chosenProductName,
          productFwdPriceA: chosenProductFwdPriceA,
        }
      ]
    }));
    console.log("넘겨받은 품목코드: " + form.placeDetails.length)
  }

	/* 모달 여닫이 */
	const openClientModal = () => {
		setClientModalOpen(true, { replace: false });
	};
	
	const closeClientModal = () => {
		setClientModalOpen(false);
				console.log("고객 코드 출력 : " + form.clientCode );
	};

	const openEmpModal = () => {
		setEmpModalOpen(true, { replace: false });
	};
	
	const closeEmpModal = () => {
		setEmpModalOpen(false);
				console.log("사원 코드 출력 : " + form.empCode );
	};

	const openStorageModal = () => {
		setStorageModalOpen(true, { replace: false });
	};
	
	const closeStorageModal = () => {
		setStorageModalOpen(false);
				console.log("창고 코드 출력 : " + form.storageCode );
	};

	const openProductModal = () => {
		setProductModalOpen(true, { replace: false });
	};
	
	const closeProductModal = () => {
		setProductModalOpen(false);
				console.log("품목 코드 출력 : " + form.productCode );
	};

	const handleChange = (e, index, key) => {
    const value = e.target.value;
    setForm(prevState => {
			const placeDetails = [...prevState.placeDetails];
			placeDetails[index][key] = value;
			placeDetails[index]["placePrice"] = placeDetails[index]["productFwdPriceA"] * placeDetails[index]["placeAmount"]
			return { ...prevState, placeDetails };
    });	
	}

  return (
		<>
		<Modal open={clientModalOpen} close={closeClientModal} header=' 거래처 선택' >
			<ClientList selectClient={selectClient} close={closeClientModal} />
		</Modal>
		<Modal open={empModalOpen} close={closeEmpModal} header=' 사원 선택' >
			<EmpList selectEmp={selectEmp} close={closeEmpModal} />
		</Modal>
		<Modal open={storageModalOpen} close={closeStorageModal} header=' 창고 선택' >
			<StorageList selectStorage={selectStorage} close={closeStorageModal} />
		</Modal>
		<Modal open={productModalOpen} close={closeProductModal} header=' 품목 선택' >
			<ProductList selectProduct={selectProduct} close={closeProductModal} />
		</Modal>
		
		<div>
			<h4>발주수정</h4>
			{ place &&
			<table border={1}>
				<tbody>
					<tr>
						<td>발주코드</td>
						<td>{ place.placeCode }</td>
					</tr>
					<tr>
						<td>발주일자</td>
						<td>{ place.placeDate }</td>
					</tr>
					<tr>
						<td>납기일지</td>
						<td>
							<input
								type="date" name="placeDelivery" onChange={ onChangeHandler }
								value={ (modifyMode ?  form.placeDelivery : place.placeDelivery) || ''}
								readOnly={ modifyMode ? false : true }
								style={ modifyMode ? null : { border: 'none', background: 'transparent'}}
							/>
						</td>
					</tr>



					<tr>
						<td>발주상태</td>
						<td>
							<label><input type="radio" name="placeStatus" onChange={ onChangeHandler } readOnly={ modifyMode ? false : true }
							checked={ (modifyMode ?  form.placeStatus : place.placeStatus) === '미확인' ? true : false } value="미확인" /> 미확인</label> &nbsp;
							<label><input type="radio" name="placeStatus" onChange={ onChangeHandler } readOnly={ modifyMode ? false : true }
							checked={ (modifyMode ? form.placeStatus : place.placeStatus) === '진행중' ? true : false } value="진행중" /> 진행중</label> &nbsp;
							<label><input type="radio" name="placeStatus" onChange={ onChangeHandler } readOnly={ modifyMode ? false : true }
							checked={ (modifyMode ? form.placeStatus : place.placeStatus) === '완료' ? true : false } value="완료" /> 완료</label>
						</td>
					</tr>
					<tr>
						<td>거래처명</td>
						<td>
							<input
								type="text" name="clientCode" onChange={ onChangeHandler } onClick={ (openClientModal ) }
								value={ (modifyMode ?  form.clientCode : place.client?.clientCode) || ''}
								readOnly={ modifyMode ? false : true }
								style={ modifyMode ? null : { border: 'none', background: 'transparent'}}
							/>
							<label readOnly={ modifyMode ? false : true } >{ (modifyMode ?  form.clientName : place.client?.clientName) || ''}</label>
						</td>
					</tr>
					<tr>
						<td>담당자명</td>
						<td>
							<input
								type="text" name="empCode" onChange={ onChangeHandler } onClick={ (openEmpModal ) }
								value={ (modifyMode ?  form.empCode : place.emp?.empCode) || ''}
								readOnly={ modifyMode ? false : true }
								style={ modifyMode ? null : { border: 'none', background: 'transparent'}}
							/>
							<label readOnly={ modifyMode ? false : true } >{ (modifyMode ?  form.empName : place.emp?.dept?.deptName+' '+place.emp?.empName+' '+place.emp?.position?.positionName) || ''}</label>
						</td>
					</tr>
					<tr>
						<td>창고명</td>
						<td>
							<input
								type="text" name="storageCode" onChange={ onChangeHandler } onClick={ (openStorageModal ) }
								value={ (modifyMode ?  form.storageCode : place.storage?.storageCode) || ''}
								readOnly={ modifyMode ? false : true }
								style={ modifyMode ? null : { border: 'none', background: 'transparent'}}
							/>
							<label readOnly={ modifyMode ? false : true } >{ (modifyMode ?  form.storageName : place.storage?.storageName) || ''}</label>
						</td>
					</tr>
				</tbody>
			</table>
			}
			<hr/>

			{place &&
			<table border={1}>
			<thead>
				<tr>

					<th>품목코드</th>
					<th>품목명</th>
					<th>발주단가</th>
					<th>발주수량</th>
					<th>금액합계</th>
					<th>비고</th>
					<th><button onClick = {onClickModifyModeHandler}> + </button></th>
				</tr>
			</thead>
			<tbody>
				{modifyMode ? (
					form.placeDetails.map((detail, index) => (

						<tr key={index}> 
							<td onClick={openProductModal} value={form.productCode}>{detail.productCode}</td>
							<td onClick={openProductModal} >{detail.productName}</td>
							<td>{detail?.productFwdPriceA}</td>
							<td><input type="number" value={detail.placeAmount} onChange={(e) => handleChange(e, index, 'placeAmount')}/></td>
							<td>{!isNaN(detail?.productFwdPriceA * detail?.placeAmount) ? (detail?.productFwdPriceA * detail?.placeAmount) : ''}</td>
							<td><input type="text" value={detail.placeNote} onChange={(e) => handleChange(e, index, 'placeNote')}/></td>
							<td>
								<button onClick={() => removePlaceDetail(index)}> - </button>	
							</td>
						</tr>
					))
				) : (
					place?.placeDetail?.map((detail, index) => (
						<tr key={index}>
							<td>{detail?.product?.productCode}</td>
							<td>{detail?.product?.productName}</td>
							<td>{detail?.product?.productFwdPriceA}</td>
							<td>{detail?.placeAmount}</td>
							<td>{!isNaN(detail?.product?.productFwdPriceA*detail?.placeAmount) ? (detail?.product?.productFwdPriceA*detail?.placeAmount) : ''}</td>
							<td>{detail?.placeNote}</td>
							<td></td>
						</tr>
					))
				)}
			</tbody>
		</table>
			}
		</div>
			<div>
				{!modifyMode && <button className='mainButton' onClick={ onClickModifyModeHandler } > 수정 </button> }
				{ modifyMode && <button className='mainButton' onClick={ onClickPlaceUpdateHandler } > 저장 </button>	}
				<button className='subButton' onClick={ () => navigate(-1) }> 이전 </button>
			</div>
		</>
	);
}

export default PlaceDetail;













