import '../../../css/Main.module.css';

import Modal from '../../../components/modal/Modal.js';

import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useState, useRef } from "react";

import ClientList from '../../../components/regist/ClientSelectList'
import EmpList from '../../../components/regist/EmpSelectList'
import StorageList from '../../../components/regist/StorageSelectList'
import ProductList from '../../../components/regist/ProductSelectList'

import { callPlaceRegistAPI } from '../../../apis/PlaceAPICalls'

function PlaceRegist() {

	const inputRef = useRef({});
  
	const dispatch = useDispatch();
	const navigate = useNavigate();

		/* 등록 핸들러 */
		const onClickPlaceRegistHandler = () => {
			console.log('[PlaceRegist] onClickPlaceRegistHandler');
			const registForm = {
				
					placeStatus: "미확인",
					placeDelivery: form.placeDelivery,
					client: {
						clientCode: form.clientCode
					},
					emp: {
						empCode: form.empCode
					},
					storage: {
						storageCode: form.storageCode
					},
					placeDetail: form.placeDetails.map(detail => ({	// placeDetail자체에 map을 걸어서 배열로 값을 받도록 한다
						placeAmount: detail.placeAmount,
						placeNote: detail.placeNote,
						product: {
							productCode: detail.productCode
						}
					}))
			}
			dispatch(callPlaceRegistAPI({ form: registForm }));
			alert('발주 등록 완료');
	}

	const [form, setForm] = useState({
		placeStatus: '',
		clientCode: 0,
		empCode: 0,
		storageCode: 0,
		placeDetails: [],		// 빈 배열을 생성
	});

		// detail 추가 버튼
		const addPlaceDetail = (placeAmount = '', placeNote = '', productCode = '') => {
			setForm(prevState => {
				const placeDetails = [...prevState.placeDetails];
				if(!placeDetails.find(detail => detail.productCode === productCode)){	//  find: placeDetail에서 값 찾는 명령어(중복값 방지)
					placeDetails.unshift({placeAmount, placeNote, productCode});		// unshift: 입력된 값이 자동으로 위로 올라가게 함
				}
				return {...prevState, placeDetails};
			});
		}
	
		// detail 삭제 버튼
		const removePlaceDetail = (index) => {
			setForm({
					...form,
					placeDetails: form.placeDetails.filter((_, i) => i !== index),
			});
	}
 
	const [clientModalOpen, setClientModalOpen] = useState(false)
	const [empModalOpen, setEmpModalOpen] = useState(false)
	const [storageModalOpen, setStorageModalOpen] = useState(false)
	const [productModalOpen, setProductModalOpen] = useState(false)

	const selectClient = (chosenClientCode, chosenClientName) => {
    setForm({...form, clientCode : chosenClientCode, clientName : chosenClientName});	// ...form을 통해 setForm에 기존 입력된 정보가 누적되도록 한다 !important
    console.log("넘겨받은 고객코드: " + form.clientCode)
	}

	const selectEmp = (chosenEmpCode, chosenEmpName) => {
    setForm({...form, empCode : chosenEmpCode, empName : chosenEmpName});
    console.log("넘겨받은 사원코드: " + form.empCode)
	}

	const selectStorage = (chosenStorageCode, chosenStorageName) => {
    setForm({...form, storageCode : chosenStorageCode, storageName : chosenStorageName});
    console.log("넘겨받은 창고코드: " + form.storageCode)
	}

	const selectProduct = (chosenProductCode, chosenProductName, chosenProductFwdPriceA) => {
    setForm(prevForm => ({		// 배열의 이전 단계 form을 받아오기 위한 구문
      ...prevForm,
      placeDetails: [
        ...prevForm.placeDetails,
        {
          productCode: chosenProductCode,
          productName: chosenProductName,
          productFwdPriceA: chosenProductFwdPriceA,
        }
      ]
    }));
    console.log("넘겨받은 품목코드: " + form.placeDetails.length)
  }

	/* 모달 여닫이 */
	const openClientModal = () => {
		setClientModalOpen(true, { replace: false });
	};
	
	const closeClientModal = () => {
		setClientModalOpen(false);
				console.log("고객 코드 출력 : " + form.clientCode );
	};

	const openEmpModal = () => {
		setEmpModalOpen(true, { replace: false });
	};
	
	const closeEmpModal = () => {
		setEmpModalOpen(false);
				console.log("사원 코드 출력 : " + form.empCode );
	};

	const openStorageModal = () => {
		setStorageModalOpen(true, { replace: false });
	};
	
	const closeStorageModal = () => {
		setStorageModalOpen(false);
				console.log("창고 코드 출력 : " + form.storageCode );
	};

	const openProductModal = () => {
		setProductModalOpen(true, { replace: false });
	};
	
	const closeProductModal = () => {
		setProductModalOpen(false);
				console.log("품목 코드 출력 : " + form.productCode );
	};

	const onChangeHandler = (e) => {
		setForm({
			...form, 
			[e.target.name]: e.target.value
		});
	};

	const handleChange = (e, index, key) => {
    const value = e.target.value;
    setForm(prevState => {
			const placeDetails = [...prevState.placeDetails];
			placeDetails[index][key] = value;
			placeDetails[index]["placePrice"] = placeDetails[index]["productFwdPriceA"] * placeDetails[index]["placeAmount"]
			return { ...prevState, placeDetails };
    });
	}

	return (
		<>
			<Modal open={clientModalOpen} close={closeClientModal} header=' 거래처 선택' >
				<ClientList selectClient={selectClient} close={closeClientModal} />
			</Modal>
			<Modal open={empModalOpen} close={closeEmpModal} header=' 사원 선택' >
				<EmpList selectEmp={selectEmp} close={closeEmpModal} />
			</Modal>
			<Modal open={storageModalOpen} close={closeStorageModal} header=' 창고 선택' >
				<StorageList selectStorage={selectStorage} close={closeStorageModal} />
			</Modal>
			<Modal open={productModalOpen} close={closeProductModal} header=' 품목 선택' >
				<ProductList selectProduct={selectProduct} close={closeProductModal} />
			</Modal>

			<div>
				<h5>발주 등록<hr/></h5>
				<table border={1}>
					<tbody>
						<tr>
							<th>거래처명</th>
							<td>
								<input type="text" name="clientCode" onChange={onChangeHandler} onClick={openClientModal} value={form.clientCode} ref={inputRef}/>
								<button onClick={openClientModal}>조회</button>
							</td>

							<th>담당자명</th>
							<td>
								<input type="text" name="empCode" onChange={onChangeHandler} onClick={openEmpModal} value={form.empCode} ref={inputRef}/>
								<button  onClick={openEmpModal}>조회</button>
							</td>
						</tr>
						
						<tr>
							<th>창고명</th>
							<td>
								<input type="text" name="storageCode"  onChange={onChangeHandler} onClick={openStorageModal} value={form.storageCode} ref={inputRef}/>
								<button onClick={openStorageModal}>조회</button>
							</td>

							<th>주문상태</th>
							<td>
								<label>미확인</label>
							</td>
						</tr>

						<tr>
							<th>납기일자</th>
							<td>
								<input type="date" name="placeDelivery" onChange={onChangeHandler} value={form.placeDelivery} ref={inputRef}/>
							</td>
						</tr>
					</tbody>
				</table>
				<button onClick={() => addPlaceDetail()}> + </button>		
				<table border={1}>
				
					<thead>
						<tr>
							<th>품목코드</th>
							<th>품목명</th>
							<th>발주단가</th>
							<th>발주수량</th>
							<th>금액합계</th>
							<th>비고</th>
							<th>삭제</th>
						</tr>
					</thead>
					<tbody>
						{form.placeDetails.map((detail, index) => (
							<tr key={index}> 
								<td onClick={openProductModal} value={form.productCode}>{detail.productCode}</td>
								<td onClick={openProductModal} >{detail.productName}</td>
								<td>{detail.productFwdPriceA}</td>
								<td><input type="number" value={detail.placeAmount} onChange={(e) => handleChange(e, index, 'placeAmount')}/></td>
								<td>{!isNaN(detail.productFwdPriceA * detail.placeAmount) ? (detail.productFwdPriceA * detail.placeAmount) : null}</td>
								<td><input type="text" value={detail.placeNote} onChange={(e) => handleChange(e, index, 'placeNote')}/></td>
								<td>
									<button onClick={() => removePlaceDetail(index)}> - </button>	
								</td>
							</tr>
						))}
					</tbody>
					
				</table>

			</div>
			<div>
				<button	className='mainButton' onClick={ onClickPlaceRegistHandler }>	등록 </button>
				<button className='subButton' onClick={ () => navigate(-1) }> 이전 </button>
			</div>
		</>
	)
}

export default PlaceRegist;