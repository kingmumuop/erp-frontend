import '../../css/Main.module.css';

import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { callStockListAPI } from '../../apis/StockAPICalls';
import { callStorageListAPI } from '../../apis/StorageAPICalls';
import { callProductListAPI } from '../../apis/ProductAPICalls';

function StockList() {

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const stocks = useSelector(state => state.stockReducer);
  const stockList = stocks.data;

  const storages = useSelector(state => state.storageReducer);
  const storageList = storages.data;

  const products = useSelector(state => state.productReducer);
  const productList = products.data;

  const pageInfo = stocks.pageInfo;

  const [currentPage, setCurrentPage]           = useState(1);
  const [search, setSearch]                     = useState('');

  useEffect(
    () => {
      dispatch(callStockListAPI({
        currentPage: currentPage
      }));
    }
    ,[currentPage]
  );

  useEffect(
    () => {
      dispatch(callStorageListAPI({
        currentPage: currentPage
      }));
    }
    ,[currentPage]
  );

  useEffect(
    () => {
      dispatch(callProductListAPI({
        currentPage: currentPage
      }));
    }
    ,[currentPage]
  );

  return (
    <>
      <div className='outlet'>
        <h4>재고</h4>
        <table style={{borderColor: '#aaaaaa', borderSpacing: 0}}  border={1}>
          <thead style={{ backgroundColor: '#266666', color: '#ffffff'}}>
            <tr>
              <th>품목코드</th>
              <th>품목명</th>
                {Array.isArray(storageList) && storageList.map((storage) => {
                  return <th key={storage.storageCode}>{storage.storageName}</th>
                })}
            </tr>
          </thead>
          <tbody>
          {Array.isArray(productList) && productList.map((product) => {
            return (
            <tr key={product?.productCode}>
              <td>{product?.productCode}</td>
              <td>{product?.productName}</td>
              {Array.isArray(storageList) && storageList.map((storage) => {
                const stock = stockList.find(stock => stock?.product?.productCode === product?.productCode && stock?.storage?.storageCode === storage?.storageCode);
                return <td key={storage?.storageCode}>{stock ? stock.stockAmount : '-'}</td>
              })}
            </tr>
            )
          })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default StockList;