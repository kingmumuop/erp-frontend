import { useDispatch } from 'react-redux';
import { useState, useRef, useEffect } from "react";
import { callApprovalRegistAPI } from '../../apis/ApprovalAPICalls';

function ScheduleList() {

  const dispatch = useDispatch();

  const [attachment, setAttachment] = useState(null);

  // const [approvalLine, setApprovalLine] = useState([4, 2, 3]);



  const [form, setForm] = useState({
    empCode: 0,
    statusCode: 0,
    docType: '',
    approvalTitle: '',
    approvalContent: '',
    attachment: '',
    link: '',
    vacationType: '',
    vacationStartDate: '',
    vacationEndDate: '',
  });

  const onChangeFileUpload = (e) => {
    const attachment = e.target.files[0];
    setAttachment(attachment);
  };

  const onChangeHandler = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

  // const onDateHandler = (e) => {
  //   setForm({
  //     ...form,
  //     [e.target.name]: new Date(e.target.value)
  //   })
  // }

  const approvalLine = [];

  const saveHandler = () => {
    const formData = new FormData();
    formData.append("empCode", form.empCode);
    formData.append("statusCode", form.statusCode);
    formData.append("docType", form.docType);
    formData.append("approvalTitle", form.approvalTitle);
    formData.append("approvalContent", form.approvalContent);
    formData.append("oldFileName", form.oldFileName);
    formData.append("newFileName", form.newFileName);
    formData.append("link", form.link);
    formData.append("vacationType", form.vacationType);

    if(form.vacationStartDate) {
      formData.append("vacationStartDate", new Date(form.vacationStartDate));
    }
    if(form.vacationEndDate) {
      formData.append("vacationEndDate", new Date(form.vacationEndDate));
    }
    if (attachment) {
      formData.append("attachment", attachment);
    }
    dispatch(callApprovalRegistAPI({
      form: formData,
      approvalLineList: approvalLine
    }));
    console.log(approvalLine);

    // alert('등록 완료');
  }



  const onClickHandler = () => {

    const num = window.prompt('결재자 사원 번호를 입력하세요');
    console.log(`입력한 숫자 ${num}`);
    
    approvalLine.push(num);
  }

  const test = () => {
    console.log(approvalLine);
  }

  return (
    <div>
      <h1>결재 등록 테스트 페이지</h1>
      <p>가장 기본적인 것부터 등록해보고 추가, 추가하는 형식으로 진행</p>

      <hr></hr>
      <p>회원코드</p>
      <input type="number" name='empCode' onChange={onChangeHandler}></input>
      <p>결재상태</p>
      <input type="number" name='statusCode' onChange={onChangeHandler}></input>
      <p>결재 제목</p>
      <input type="text" name='approvalTitle' onChange={onChangeHandler}></input>
      <p>결재 내용</p>
      <input type="text" name='approvalContent' onChange={onChangeHandler}></input>
      <p>링크</p>
      <input type="text" name='link' onChange={onChangeHandler}></input>
      <hr></hr>

      <div>
        <input
yy          type="file"
          name='attachment'
          onChange={onChangeFileUpload}
        />
      </div>

      <p>휴가 구분</p>
      <input type="text" name='vacationType' onChange={onChangeHandler}></input>
      <p>휴가시작일</p>
      <input type="date" name='vacationStartDate' onChange={onChangeHandler}></input>
      <p>휴가종료일</p>
      <input type="date" name='vacationEndDate' onChange={onChangeHandler}></input>
      <hr></hr>
      <h1>결재라인</h1>
      <div>{approvalLine}</div>
      <div onClick={onClickHandler} style={{width:100, height: 100, textAlign:'center', backgroundColor: 'pink', alignItems:'center'}}>+</div>
      <button onClick={saveHandler}>저장</button>
      <button onClick={test}>확인하기</button>
    </div>
  );
}

export default ScheduleList;