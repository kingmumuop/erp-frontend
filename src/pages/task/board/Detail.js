import "../../../css/boardRegistStyle.css";
import { useEffect} from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import {
  callBoardDetailAPI,
} from "../../../apis/BoardAPICalls";

function BoardUpdate() {
  const dispatch = useDispatch();
  const params = useParams();
  const boardDetail = useSelector((state) => state.boardReducer);
  const navigate = useNavigate();

  useEffect(() => {
    console.log("[BoardUpdate] boardCode : ", params.boardCode);

    dispatch(
      callBoardDetailAPI({
        boardCode: params.boardCode,
      })
    );
  }, []);


  /* 시간  */
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear() % 2000;

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("/");
  }

  /* 첨부파일 열기 */
  const fileView = () => {
    window.open(boardDetail.newFileName, 'file', 'width= 500, height=500, left=400, top=100')
  }

  return (
    <>
      {boardDetail && (
        <div className="boardregist">
          <h3>게시글 수정</h3>
          <table border={1}>
            <tr className="board">
              <th>게시판 구분</th>
              <td colSpan={2}>
                {boardDetail.boardDiv}
              </td>
              <th>공지 설정</th>
              <td>
                {boardDetail.noticeYn}
              </td>
            </tr>
            <tr className="writer">
              <th>작성자</th>
              <td className="s1">
                {boardDetail.anonymousYn === "익명"
                  ? "***"
                  : boardDetail.emp?.empName}
              </td>
              <td className="s2"></td>
              <th>게시 종료일</th>
              <td>{formatDate(boardDetail.expireDate)}</td>
            </tr>
            <tr className="title1">
              <th>제목</th>
              <td colSpan={4}>{boardDetail.boardTitle}</td>
            </tr>
            <tr className="detail">
              <th>내용</th>
              <td colSpan={4}>{boardDetail.boardContent}</td>
            </tr>
            <tr className="lock">
              <th>잠금 설정</th>
              <td className="s4" colSpan={2}>
                비밀 글
              </td>
              <td className="s5" colSpan={2}>
                <label>비밀번호</label>
                <input type="password"></input>
              </td>
            </tr>
            <tr className="file">
              <th>첨부파일</th>
              <td colSpan={4}>
              {(boardDetail.oldFileName) ?
                      <p onClick={fileView}>{boardDetail?.oldFileName}</p> :
                      <p style={{ color: 'gray' }}>첨부파일 없음</p>}
              </td>
            </tr>
          </table>
          <div>
            <button onClick={() => navigate(-1)}>돌아가기</button>
          </div>
        </div>
      )}
    </>
  );
}

export default BoardUpdate;
