import "../../../css/boardListStyle.css";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import clip from '../../../images/clip.png';
import approval from '../../../images/approval.png';
import {
  callBoardListAPI,
  callBoardDetailAPI,
  callMyBoardListAPI
} from "../../../apis/BoardAPICalls";

function BoardList() {
  const dispatch = useDispatch();
  const boards = useSelector((state) => state.boardReducer);
  const boardList = boards.data;
  const navigate = useNavigate();
  const pageInfo = boards.pageInfo;

  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const [isTrue, setIsTrue] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [isMine, setIsMine] = useState(true);

  const pageNumber = [];
  if (pageInfo) {
    for (let i = 1; i <= pageInfo.pageEnd; i++) {
      pageNumber.push(i);
    }
  }

  const onSelectHandler = (e) => {
    setIsTrue(false);
    const input = document.getElementsByTagName("input");
    // console.log(input.length);
    for (var i = input.length - 1; i >= 0; i--) {
      if (input[0].checked === true) {
        input[i].checked = true;
      } else {
        input[i].checked = false;
      }
    }
  };

  /* 시간  */
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear() % 2000;

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("/");
  }

  /* 전체 조회 */
  useEffect(() => {
    dispatch(
      callBoardListAPI({
        currentPage: currentPage,
      })
    );
  }, [currentPage]);

  /* 수정 */
  const onClickTableTr = (boardCode) => {
    dispatch(
      callBoardDetailAPI({
        boardCode: boardCode,
      })
    );
    navigate(`/task/board/detail/${boardCode}`, { replace: false });
  };

  const onClickBoardInsert = () => {
    console.log("[BoardList] onClickBoardInsert");
    navigate("/task/board/regist", { replace: false });
  };

  const onClickMyList = () => {
    navigate("/task/board/myList", {replace: false });
  }



    /* 전체 리스트 보기 */
    const allBoardHandler = () => {
      setIsMine(false);
      const checkbox = document.getElementsByClassName('check');
      for (var i = checkbox.length - 1; i >= 0; i--) {
        checkbox[i].checked = false;
      }
      dispatch(callBoardListAPI({
        currentPage: currentPage
      }));
    };

      /* 내 게시판보기 버튼 핸들러 */
  // const myBoardHandler = () => {
  //   setIsMine(true);
  //   const checkbox = document.getElementsByClassName('check');
  //   for (var i = checkbox.length - 1; i >= 0; i--) {
  //     checkbox[i].checked = false;
  //   }
  //   dispatch(callMyBoardListAPI({
  //     currentPage: currentPage,
  //     empCode: 1                        // token으로 변경 필요
  //   }));
  // };
  return (
    <div className="boardlist">
      {/* <h3>게시판관리</h3>

      {isMine === true ? (
        <div className="approvalInfo">
          <div>
            <img src={user} className="approvalImg" />
            <p>내가 작성한 를 관리합니다.</p>
          </div>
          <button onClick={allBoardHandler}>전체 결제 보기</button>
        </div>
      ) : (
        <div className="approvalInfo">
          <div>
            <img src={approval} className="approvalImg" />
            <p>
              전체 게시판 목록입니다
            </p>
          </div>
          <button onClick={myBoardHandler}>내 게시판 보기</button>
        </div>
      )} */}





      <div
        style={{
          listStyleType: "none",
          display: "flex",
          justifyContent: "left",
        }}
      >
        {Array.isArray(boardList) && (
          <button
            className="activeButton"
            onClick={() => setCurrentPage(currentPage - 1)}
            disabled={currentPage === 1}
          >
            &lt;
          </button>
        )}

        {pageNumber.map((num) => (
          <li key={num} onClick={() => setCurrentPage(num)}>
            <button
              className="inactiveButton"
              style={
                currentPage === num ? { backgroundColor: "#E8EBE7" } : null
              }
            >
              {num}
            </button>
          </li>
        ))}
        {Array.isArray(boardList) && (
          <button
            className="activeButton"
            onClick={() => setCurrentPage(currentPage + 1)}
            disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
          >
            &gt;
          </button>
        )}
      </div>
      <h3>자유게시판</h3>
      <table>
        <thead>
          <tr>
          
            <th>No</th>
            <th>제시글제목</th>
            <th>게시글내용</th>
            <th>참부파일</th>
            <th>게시일자</th>
            <th>작성자</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(boardList) &&
            boardList.map((board) => (
              <tr key={board.boardCode}>
               
                <td
                  className="boardCode"
                  onClick={() => onClickTableTr(board.boardCode)}
                >
                  {board.boardCode}
                </td>
                <td>{board.boardTitle}</td>
                <td>{board.boardContent}</td>
                <td>{board.oldFileName}</td>
               
                <td>{formatDate(board.boardDate)}</td>
                <td>
                  {board.anonymousYn === "익명" ? "***" : board.emp?.empName}
                </td>
              </tr>
            ))}
        </tbody>
      </table>
      <button onClick={onClickBoardInsert}>글쓰기</button>
      <button onClick={onClickMyList}>내글 보기</button>
    </div>
  );
}

export default BoardList;
