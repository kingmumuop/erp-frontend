
import "../../../css/boardListStyle.css";
import { useNavigate , useParams} from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";

import { callMyListAPI, callBoardDetailAPI} from "../../../apis/BoardAPICalls";

function MyBoardList() {
  const dispatch = useDispatch();
  const params = useParams();
  const boards = useSelector((state) => state.boardReducer);
  const MyBoardList = boards.data;
  const navigate = useNavigate();
  const pageInfo = boards.pageInfo;
  const loginInfo = useSelector(state => state.loginReducer);

  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const [isTrue, setIsTrue] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [isMine, setIsMine] = useState(true);


   const pageNumber = [];
  if (pageInfo) {
    for (let i = 1; i <= pageInfo.pageEnd; i++) {
      pageNumber.push(i);
    }
  }

  const onSelectHandler = (e) => {
    setIsTrue(false);
    const input = document.getElementsByTagName("input");
    // console.log(input.length);
    for (var i = input.length - 1; i >= 0; i--) {
      if (input[0].checked === true) {
        input[i].checked = true;
      } else {
        input[i].checked = false;
      }
    }
  };

  /* 전체 조회 */
  useEffect(() => {
    dispatch(
      callMyListAPI({
        currentPage: currentPage,
        empCode:loginInfo.empCode
      })
    );
  }, [loginInfo.empCode,currentPage]);


  
  /* 수정 */
  const onClickTableTr = (boardCode) => {
    dispatch(
      callBoardDetailAPI({
        boardCode: boardCode,
      })
    );
    navigate(`/task/board/myDetail/${boardCode}`, { replace: false });
  };

  
  /* 시간  */
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear() % 2000;

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("/");
  }


  return (
    <div className="boardlist">
    <div
        style={{
          listStyleType: "none",
          display: "flex",
          justifyContent: "left",
        }}
      >
        {Array.isArray(MyBoardList) && (
          <button
            className="activeButton"
            onClick={() => setCurrentPage(currentPage - 1)}
            disabled={currentPage === 1}
          >
            &lt;
          </button>
        )}

        {pageNumber.map((num) => (
          <li key={num} onClick={() => setCurrentPage(num)}>
            <button
              className="inactiveButton"
              style={
                currentPage === num ? { backgroundColor: "#E8EBE7" } : null
              }
            >
              {num}
            </button>
          </li>
        ))}
        {Array.isArray(MyBoardList) && (
          <button
            className="activeButton"
            onClick={() => setCurrentPage(currentPage + 1)}
            disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
          >
            &gt;
          </button>
        )}
      </div>
      <h3>내 게시판</h3>
      <table>
        <thead>
          <tr>
            <th style={{ width: 5 }}>
              
            </th>
            <th>No</th>
            <th>제시글제목</th>
            <th>게시글내용</th>
            <th>참부파일</th>
            <th>게시일자</th>
            <th>작성자</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(MyBoardList) &&
            MyBoardList.map((board) => (
              <tr key={board.boardCode}>
                <td>
                  <input type="checkbox" />
                </td>
                <td
                  className="boardCode"
                  onClick={() => onClickTableTr(board.boardCode)}
                >
                  {board.boardCode}
                </td>
                <td>{board.boardTitle}</td>
                <td>{board.boardContent}</td>
                <td>{board.oldFileName}</td>
                <td>{formatDate(board.boardDate)}</td>
                <td>
                  {board.anonymousYn === "익명" ? "***" : board.emp?.empName}
                </td>
              </tr>
            ))}
        </tbody>
      </table>
      </div>
  );
}

export default MyBoardList;
