import "../../../css/boardRegistStyle.css";
// import { decodeJwt } from "../utils/tokenUtils";
import { useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import {
  callBoardDetailAPI,
  callBoardUpdateAPI,
  callBoardDeleteAPI
} from "../../../apis/BoardAPICalls";

function MyBoardDetail() {
  const dispatch = useDispatch();
  const params = useParams();
  const boardDetail = useSelector((state) => state.boardReducer);

  const [modifyMode, setModifyMode] = useState(false);
  const [image, setImage] = useState(null);
  const imageInput = useRef();
  const [imageUrl, setImageUrl] = useState(null);
  const navigate = useNavigate();

  const [form, setForm] = useState({});

  useEffect(() => {
    console.log("[BoardUpdate] boardCode : ", params.boardCode);

    dispatch(
      callBoardDetailAPI({
        boardCode: params.boardCode,
      })
    );
  }, []);

  useEffect(() => {
        
    /* 이미지 업로드시 미리보기 세팅 */
    if(image){
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
            const { result } = e.target;
            if( result ){
                setImageUrl(result);
            }
        }
        fileReader.readAsDataURL(image);
    }
},
[image]);





  const onClickModifyModeHandler = () => {
    // 수정모드
    setModifyMode(true);
    setForm({
      boardCode: boardDetail.boardCode,
      // empCode: boardDetail.empCode,
      boardTitle: boardDetail.boardTitle,
      boardContent: boardDetail.boardContent,
      noticeYn: boardDetail.noticeYn,
      expireDate: boardDetail.expireDate,
      boardPwd: boardDetail.boardPwd,
      anonymousYn: boardDetail.anonymousYn,
      oldFileName: boardDetail.oldFileName,
      newFileName: boardDetail.newFileName,
      boardDiv: boardDetail.boardDiv,
    });
  };

  /* form 데이터 세팅 */
  const onChangeHandler = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const onClickBoardUpdateHandler = () => {
    console.log("[BoardUpdate] onClickBoardUpdateHandler");

    const formData = new FormData();
    formData.append("boardCode", form.boardCode);
    // formData.append("empCode", form.empCode);
    formData.append("boardTitle", form.boardTitle);
    formData.append("boardContent", form.boardContent);
    formData.append("noticeYn", form.noticeYn);
    formData.append("boardPwd", form.boardPwd);
    formData.append("anonymousYn", form.anonymousYn);
    formData.append("oldFileName", form.oldFileName);
    formData.append("newFileName", form.newFileName);
    formData.append("boardDiv", form.boardDiv);

    
    if(form.boardDate) {
      formData.append("boardDate", new Date(form.boardDate));
    }
    if(form.expireDate) {
      formData.append("expireDate", new Date(form.expireDate));
    }

    if(image){
      formData.append("BoardImage", image);
  }

    dispatch(
      callBoardUpdateAPI({
        // 상품 정보 업데이트
        form: formData,
      })
    );
    console.log(form);

    alert("게시판을 수정했습니다.");
    navigate("/task/board/MyList", { replace: true });
  };



  


   /* 첨부파일 열기 */
   const fileView = () => {
    window.open(boardDetail.newFileName, 'file', 'width= 500, height=500, left=400, top=100')
  }

  /* 시간  */
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear() % 2000;

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("/");
  }

  return (
    <>
      {boardDetail && (
        <div className="boardregist">
          <h3>게시글 수정</h3>
          <table border={1}>
            <tr className="board">
              <th>게시판 구분</th>
              <td colSpan={2}>
                  <label>
                <input
                  type="radio"
                  name="boardDiv"
                  placeholder="게시판 구분"
                  checked={
                    (!modifyMode ? boardDetail.boardDiv : form.boardDiv) == "신고"? true : false
                  }
                  value="신고"
                  onChange={onChangeHandler}
                  readOnly={modifyMode ? false : true}
                  style={!modifyMode ? { backgroundColor: "gray" } : null}
                />신고</label>&nbsp;
                <label>
                 <input
                  type="radio"
                  name="boardDiv"
                  placeholder="게시판 구분"
                  checked={
                    (!modifyMode ? boardDetail.boardDiv : form.boardDiv) == "자유게시판"? true : false
                  }
                  value="자유게시판"
                  onChange={onChangeHandler}
                  readOnly={modifyMode ? false : true}
                  style={!modifyMode ? { backgroundColor: "gray" } : null}
                />자유게시판</label>
              </td>

              <th>공지 설정</th>
              <td>
                <label>
                  <input
                    type="radio"
                    name="noticeYn"
                    onChange={onChangeHandler}
                    readOnly={modifyMode ? false : true}
                    checked={
                      (!modifyMode ? boardDetail.noticeYn : form.noticeYn) ==
                      "공지"
                        ? true
                        : false
                    }
                    value="공지"
                  />{" "}
                  공지{" "}
                </label>{" "}
                &nbsp;
                <label>
                  <input
                    type="radio"
                    name="noticeYn"
                    onChange={onChangeHandler}
                    readOnly={modifyMode ? false : true}
                    checked={
                      (!modifyMode ? boardDetail.noticeYn : form.noticeYn) ==
                      "비공지"
                        ? true
                        : false
                    }
                    value="비공지"
                  />{" "}
                  비공지
                </label>{" "}
                &nbsp;
              </td>
            </tr>
            <tr className="writer">
              <th>작성자</th>
              <td className="s1">
                {boardDetail.anonymousYn === "익명"
                  ? "***"
                  : boardDetail.emp?.empName}
              </td>
              <td className="s2">
                {/* <label><input type="radio" name="anonymousYn" onChange={ onChangeHandler } readOnly={ modifyMode ? false : true } checked={ (!modifyMode ? boardDetail.anonymousYn : form.anonymousYn) == '익명' ? true : false } value="익명"/>익명</label>
              <label><input type="radio" name="anonymousYn" onChange={ onChangeHandler } readOnly={ modifyMode ? false : true } checked={ (!modifyMode ? boardDetail.anonymousYn : form.anonymousYn) == '공개' ? true : false } value="공개"/>공개</label> */}
              </td>
              <th>게시 종료일</th>
              <td>
                {!modifyMode ? (
                  formatDate(boardDetail.expireDate)
                ) : (
                  <input
                    style={!modifyMode ? { backgroundColor: "gray" } : null}
                    name="expireDate"
                    placeholder="게시 종료일"
                    type="date"
                    onChange={onChangeHandler}
                  />
                )}
              </td>
            </tr>
            <tr className="title1">
              <th>제목</th>
              <td colSpan={4}>
                <input
                  style={!modifyMode ? { backgroundColor: "gray" } : null}
                  type="text"
                  name="boardTitle"
                  placeholder="제목을 입력하세요"
                  value={
                    (!modifyMode ? boardDetail.boardTitle : form.boardTitle) ||
                    ""
                  }
                  onChange={onChangeHandler}
                  readOnly={modifyMode ? false : true}
                />
              </td>
            </tr>
            <tr className="detail">
              <th>내용</th>
              <td colSpan={4}>
                <input
                  type="text"
                  style={!modifyMode ? { backgroundColor: "gray" } : null}
                  name="boardContent"
                  placeholder="내용을 입력하세요"
                  value={
                    (!modifyMode
                      ? boardDetail.boardContent
                      : form.boardContent) || ""
                  }
                  onChange={onChangeHandler}
                  readOnly={modifyMode ? false : true}
                />
              </td>
            </tr>
            <tr className="lock">
              <th>잠금 설정</th>
              <td className="s4" colSpan={2}>
                <input
                  style={!modifyMode ? { backgroundColor: "gray" } : null}
                  type="checkbox"
                  id="pass"
                  name="checkbox3"
                  value="비밀 글"
                />
                비밀 글
              </td>
              <td className="s5" colSpan={2}>
                <label>비밀번호</label>
                <input type="password"></input>
              </td>
            </tr>
            <tr className="file">
              <th>첨부파일</th>
              <td colSpan={4}>
              {(boardDetail.oldFileName) ?
                      <p onClick={fileView}>{boardDetail?.oldFileName}</p> :
                      <p style={{ color: 'gray' }}>첨부파일 없음</p>}
                
              </td>
            </tr>
          </table>
          <div>
            <button onClick={() => navigate(-1)}>돌아가기</button>
            {!modifyMode && (
              <button onClick={onClickModifyModeHandler}>수정모드</button>
            )}
            {modifyMode && (
              <button onClick={onClickBoardUpdateHandler}>
                상품 수정 저장하기
              </button>
            )}
          </div>
        </div>
      )}
    </>
  );
}

export default MyBoardDetail;
