import '../../../css/Stamp.css'
import stamp from '../../../images/stamp.png'
import { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { callStampRegistAPI } from '../../../apis/LoginAPICalls';

function Stamp() {

  const dispatch = useDispatch();
  const [image, setImage] = useState(null);
  const [imageUrl, setImageUrl] = useState();
  const imageInput = useRef();

  const loginInfo = useSelector(state => state.loginReducer);

  useEffect(() => {
    if (image) {
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        const { result } = e.target;
        if (result) {
          setImageUrl(result);
        }
      }
      fileReader.readAsDataURL(image);
    }
  }, [image]);

  const onChangeImageUpload = (e) => {
    const image = e.target.files[0];
    setImage(image);
  };

  const onStampSaveHandler = () => {
    console.log('[Stamp] onStampSaveHandler');
    const formData = new FormData();
    if (image) {
      formData.append("stampImage", image);
      dispatch(callStampRegistAPI({
        empCode: loginInfo.empCode,
        form: formData
      }));
      window.location.reload();
      alert('저장되었습니다.');
      window.location.reload();
    } else {
      alert('파일을 선택해주세요.');
    }
    console.log(image);
  }

  const onClickImgView = () => {
    window.open(loginInfo.empStamp, 'stamp', 'width= 500, height=500, left=400, top=100')
  }

  return (
    <div className="stampRegist">
      <h3>도장등록</h3>

      {/* <img src={stamp} className="stampSize" /> */}

      {(imageUrl) ? 
        <img className="stampSize" src={imageUrl} alt="preview"/> :
        <img className="stampSize" src={stamp}/>
      }
      <input
        type='file'
        name='empStamp'
        accept='image/jpg,image/png,image/jpeg,image/gif'
        onChange={onChangeImageUpload}
        ref={imageInput}
      />
      {/* {(imageUrl) ? <p></p> : <p>등록된 도장이 없습니다. 도장을 등록해주세요.</p>} */}
      <button onClick={onStampSaveHandler}>저장</button>

      <h1>내 도장</h1>
      <div>
        {((loginInfo.empStamp !== "http://localhost:7777/stampimgs/null") &&
         <img src={loginInfo?.empStamp} onClick={onClickImgView} className='myStamp'/>) || 
         <p>등록된 도장이 없습니다. 도장을 등록해주세요</p>}
      </div>

    </div>
  );
}

export default Stamp;