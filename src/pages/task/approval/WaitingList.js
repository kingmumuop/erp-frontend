import ApprovalStyle from '../../../css/Approval.module.css';
import on from '../../../images/on.png';
import off from '../../../images/off.png';
import waiting from '../../../images/waiting.png';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import ApprovalTable from '../../../components/task/ApprovalTable';
import ApprovalLineList from '../../../components/task/ApprovalLineList';
import { callWaitingProcessListAPI } from '../../../apis/ApprovalAPICalls';

function WaitingList() {

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  const [isLineClicked, setIsLineClicked] = useState(false);
  const isApprover = true;

  const approvals = useSelector(state => state.approvalReducer);
  const loginInfo = useSelector(state => state.loginReducer);
  const approvalList = approvals.data;
  const pageInfo = approvals.pageInfo;

  const pageNumber = [];
  if (pageInfo) {
    for (let i = 1; i <= pageInfo.pageEnd; i++) {
      pageNumber.push(i);
    }
  };

  useEffect(
    () => {
      dispatch(callWaitingProcessListAPI({
        currentPage: currentPage,
        empCode: loginInfo.empCode
      }));
    }
    , [currentPage]
  );

  /* 내가 승인했던 서류 조회 */
  const doneProcessHandler = () => {
    navigate('/task/approval/done')
  }

  /* 결재라인 테이블 토글 */
  const onLineOpenHandler = () => {
    setIsLineClicked(true);
  };
  const onLineCloseHandler = () => {
    setIsLineClicked(false);
  };

  return (
    <div className={ApprovalStyle.outlet}>
      <h3>대기중인 결재</h3>

      <div className={ApprovalStyle.approvalInfo}>
        <div>
          <img src={waiting} className={ApprovalStyle.approvalImg} />
          <p>나의 결재를 기다리는 기안서 목록을 표시합니다.</p>
        </div>
        <button onClick={doneProcessHandler}>결재완료 서류보기</button>
      </div>

      <div className={ApprovalStyle.pageButton}>
        {Array.isArray(approvalList) &&
          <button
            className={ApprovalStyle.activeButton}
            onClick={() => setCurrentPage(currentPage - 1)}
            disabled={currentPage === 1} > &lt; </button>
        }
        {pageNumber.map((num) => (
          <li key={num} onClick={() => setCurrentPage(num)}>
            <button
              className={ApprovalStyle.inactiveButton}
              style={currentPage === num ? { backgroundColor: '#E8EBE7' } : null} > {num} </button>
          </li>
        ))}
        {Array.isArray(approvalList) &&
          <button
            className={ApprovalStyle.activeButton}
            onClick={() => setCurrentPage(currentPage + 1)}
            disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
          > &gt; </button>
        }
      </div>

      <ApprovalTable approvalList={approvalList} setIsLineClicked={setIsLineClicked} isApprover={isApprover}/>

      {isLineClicked ?
        <div>
          <img
            src={on}
            onClick={onLineCloseHandler}
            className={ApprovalStyle.toggleImg} />
          <ApprovalLineList />
        </div> :
        <img
          src={off}
          onClick={onLineOpenHandler}
          className={ApprovalStyle.toggleImg}
        />
      }
    </div>
  );
}

export default WaitingList;