import ApprovalStyle from '../../../css/Approval.module.css';
import '../../../css/DeptList.css';
import searchImg from '../../../images/search.png';
import user from '../../../images/user.png';
import on from '../../../images/on.png';
import off from '../../../images/off.png';
import pen from '../../../images/pen.png';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import ApprovalLineList from '../../../components/task/ApprovalLineList';
import ApprovalTable from '../../../components/task/ApprovalTable';
import { callApprovalsDeleteAPI, callMyApprovalListAPI } from '../../../apis/ApprovalAPICalls';

function ApprovalList() {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const approvals = useSelector(state => state.approvalReducer);
  const approvalList = approvals.data;
  const pageInfo = approvals.pageInfo;

  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState('');
  const [isLineClicked, setIsLineClicked] = useState(false);

  const loginInfo = useSelector(state => state.loginReducer);

  const pageNumber = [];
  if (pageInfo) {
    for (let i = 1; i <= pageInfo.pageEnd; i++) {
      pageNumber.push(i);
    }
  };

  useEffect(
    () => {
      dispatch(callMyApprovalListAPI({
        currentPage: currentPage,
        empCode: loginInfo.empCode                    // token으로 변경 필요
      }));
    }
    , [currentPage]
  );

  /* 결재라인 테이블 토글 */
  const onLineOpenHandler = () => {
    setIsLineClicked(true);
  };
  const onLineCloseHandler = () => {
    setIsLineClicked(false);
  };

  /* 검색 기능 */
  const onSearchChangeHandler = (e) => {
    setSearch(e.target.value);
  }
  const onEnterkeyHandler = (e) => {
    if (e.key === 'Enter') {
      console.log('Enter key', search);
      navigate(`/task/approval/search?value=${search}`, { replace: false });
      window.location.reload();
    }
  };
  const onSearchClickHandler = () => {
    navigate(`/task/approval/search?value=${search}`, { replace: false });
    window.location.reload();
  };

  /* 전체 리스트 보기 */
  const allApprovalHandler = () => {
    navigate('/task/approval/list')
  };

  /* 삭제 버튼 핸들러 */
  const onDeleteHandler = () => {
    const selectedApproval = document.getElementsByClassName('check');
    const approvalCodes = [];
    for (var i = selectedApproval.length - 1; i >= 0; i--) {
      if (selectedApproval[i].checked === true) {
        if (selectedApproval[i].parentElement.nextElementSibling.innerHTML !== "기안일시") {
          const num = selectedApproval[i].parentElement.parentElement.lastChild.innerHTML;
          approvalCodes.push(Number(num));
          console.log(approvalCodes);
        }
      }
    }
    if (approvalCodes.length > 0) {
      const answer = window.confirm(approvalCodes + '번 결재를 정말 삭제하시겠습니까?');
      if (answer === true) {
        dispatch(callApprovalsDeleteAPI({ approvalCodes }));
        window.location.reload();
      } else {
        return false;
      }
    } else {
      alert('삭제할 결재를 선택해주세요.');
    }
  }

  const onNewWriteHandler = () => {
    navigate('/task/approval/regist');
  }

  return (
    <div className={ApprovalStyle.outlet}>
      <h3>나의 결재목록</h3>
      <div className={ApprovalStyle.approvalInfo}>
        <div>
          <img src={user} className={ApprovalStyle.approvalImg} />
          <p>내가 작성한 결재를 관리합니다.</p>
        </div>
        <button onClick={allApprovalHandler}>전체 결제 보기</button>
      </div>

      <div className={ApprovalStyle.right}>
        <input
          type='text'
          placeholder="내 결재 범위 제목으로 검색"
          value={search}
          onKeyUp={onEnterkeyHandler}
          onChange={onSearchChangeHandler}
        />
        <img
          onClick={onSearchClickHandler}
          src={searchImg}
          className={ApprovalStyle.imgButton}
        />
      </div>

      <button onClick={onDeleteHandler}>삭제</button>

      <ApprovalTable approvalList={approvalList} setIsLineClicked={setIsLineClicked} />

      <div className={ApprovalStyle.pageButton}>
        {Array.isArray(approvalList) &&
          <button
            className={ApprovalStyle.activeButton}
            onClick={() => setCurrentPage(currentPage - 1)}
            disabled={currentPage === 1} > &lt; </button>
        }
        {pageNumber.map((num) => (
          <li key={num} onClick={() => setCurrentPage(num)}>
            <button
              className={ApprovalStyle.inactiveButton}
              style={currentPage === num ? { backgroundColor: '#E8EBE7' } : null} > {num} </button>
          </li>
        ))}
        {Array.isArray(approvalList) &&
          <button
            className={ApprovalStyle.activeButton}
            onClick={() => setCurrentPage(currentPage + 1)}
            disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
          > &gt; </button>
        }
      </div>
      <div className={ApprovalStyle.writeNew}>
        <p onClick={onNewWriteHandler}>새 글 작성하기</p>
        <img src={pen} className={ApprovalStyle.penImg} onClick={onNewWriteHandler}/>
      </div>

      {isLineClicked ?
        <div>
          <img
            src={on}
            onClick={onLineCloseHandler}
            className={ApprovalStyle.toggleImg} />
          <ApprovalLineList />
        </div> :
        <img
          src={off}
          onClick={onLineOpenHandler}
          className={ApprovalStyle.toggleImg}
        />
      }
    </div>
  );
}

export default ApprovalList;