import '../../../css/DeptList.css'
import queryString from 'query-string';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import back from '../../../images/back.png';
import { callSearchApprovalAPI } from '../../../apis/ApprovalAPICalls';
import { callApprovalLineAPI } from '../../../apis/ApprovalLineAPICalls';
import ApprovalLineList from '../../../components/task/ApprovalLineList'
import clip from '../../../images/clip.png';

function ApprovalSearch() {

  const navigate = useNavigate();
  const { search } = useLocation();
  const { value } = queryString.parse(search);
  const approvals = useSelector(state => state.approvalReducer);
  const [isLineClicked, setIsLineClicked] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(callSearchApprovalAPI({
      search: value
    }));
  },
    []);

  /* 체크 선택 핸들러 */
  const onSelectHandler = (e) => {
    const input = document.getElementsByTagName('input');
    for (var i = input.length - 1; i >= 0; i--) {
      if (input[0].checked === true) {
        input[i].checked = true;
      } else {
        input[i].checked = false;
      }
    }
  }

  function formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = (d.getFullYear()) % 2000;
    // hour = '' + (d.getHours()),
    // minute = '' + (d.getMinutes()),
    // second = '' + (d.getSeconds());

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
    // return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':');
  }

  const onDetailHandler = (code) => {
    navigate(`/task/approval/detail/${code}`, { replace: false })
  }

  const onLineClickHandler = (approvalCode) => {
    console.log(approvalCode);
    setIsLineClicked(true);
    {
      dispatch(callApprovalLineAPI({
        approvalCode: approvalCode
      }))
    }
  };

  return (
    <div className='outlet'>

      <h4>검색결과</h4>
      <div className='right' onClick={() => navigate(-1)}>
        <p>돌아가기</p>
        <img src={back} className='imgButton' />
      </div>


      <table border={1} className='deptTable'>
        <thead>
          <tr>
            <th style={{ width: 50 }}><input type='checkbox' onChange={onSelectHandler} /></th>
            <th>기안일시</th>
            <th>구분</th>
            <th style={{ width: 300, borderRight: 'none' }}>제목</th>
            <th style={{ width: 20, borderLeft: 'none' }}></th>
            <th>기안부서</th>
            <th>기안자</th>
            <th>상태</th>
            <th style={{ width: 50 }}>문서번호</th>
            <th>디테일</th>
          </tr>
        </thead>
        <tbody>
          {approvals.length > 0 && approvals.map((approval) => (
            <tr
              key={approval.approvalCode}
              onClick={() => onLineClickHandler(approval.approvalCode)}
            >

              <td><input type='checkbox' /></td>
              <td>{formatDate(approval.approvalDate)}</td>
              <td>{approval.docType}</td>
              <td style={{ borderRight: 'none' }}>{approval.approvalTitle}</td>
              <td style={{ borderLeft: 'none' }}>{(approval.oldFileName !== null) ? <img src={clip} style={{ width: 15, height: 15 }} /> : ''}</td>
              {/* <td>{ approval.oldFileName}</td> */}
              <td>{approval.emp?.dept?.deptName}</td>
              <td>{approval.emp?.empName}</td>

              <td>{approval.approvalStatus?.statusDesc}</td>
              <td>{approval.approvalCode}</td>
              <td><button onClick={() => onDetailHandler(approval.approvalCode)}>디테일</button></td>
            </tr>
          ))
          }
        </tbody>
      </table>

      {isLineClicked ? <ApprovalLineList /> : undefined}
    </div>
  );
}

export default ApprovalSearch;