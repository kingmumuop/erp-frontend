import ApprovalStyle from '../../../css/Approval.module.css';
import draft from '../../../images/draft.png';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { callMyDraftListAPI, callApprovalsDeleteAPI } from '../../../apis/ApprovalAPICalls';
import { callApprovalLineAPI } from '../../../apis/ApprovalLineAPICalls';
import ApprovalTable from '../../../components/task/ApprovalTable';

function ApprovalStorage() {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const approvals = useSelector(state => state.approvalReducer);
  const approvalList = approvals.data;
  const pageInfo = approvals.pageInfo;
  const loginInfo = useSelector(state => state.loginReducer);

  const [currentPage, setCurrentPage] = useState(1);

  const pageNumber = [];
  if (pageInfo) {
    for (let i = 1; i <= pageInfo.pageEnd; i++) {
      pageNumber.push(i);
    }
  };

  useEffect(
    () => {
      dispatch(callMyDraftListAPI({
        currentPage: currentPage,
        empCode: loginInfo.empCode                        // token으로 변경 필요
      }));
    }
    , [currentPage]
  );

  /* 체크 선택 핸들러 */
  const onSelectHandler = (e) => {
    const input = document.getElementsByTagName('input');
    for (var i = input.length - 1; i >= 0; i--) {
      if (e.target.checked === true) {
        input[i].checked = true;
      } else {
        input[i].checked = false;
      }
    }
  }

  /* 행 클릭 핸들러 */
  const onLineClickHandler = (approvalCode) => {
    console.log(approvalCode);
    {
      dispatch(callApprovalLineAPI({
        approvalCode: approvalCode
      }))
    }
  };

  /* 날짜 형식 */
  function formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = (d.getFullYear()) % 2000;
    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;
    return [year, month, day].join('-');
  }

  /* 상세페이지로 이동 */
  const onDetailHandler = (code) => {
    navigate(`/task/approval/detail/${code}`, { replace: false })
  }

  /* 삭제 버튼 핸들러 */
  const onDeleteHandler = () => {
    const selectedApproval = document.getElementsByClassName('check');
    const approvalCodes = [];
    for (var i = selectedApproval.length - 1; i >= 0; i--) {
      if (selectedApproval[i].checked === true) {
        if (selectedApproval[i].parentElement.nextElementSibling.innerHTML !== "기안일시") {
          const num = selectedApproval[i].parentElement.parentElement.lastChild.innerHTML;
          approvalCodes.push(Number(num));
          console.log(approvalCodes);
        }
      }
    }
    if (approvalCodes.length > 0) {
      const answer = window.confirm(approvalCodes + '번 결재를 정말 삭제하시겠습니까?');
      if (answer === true) {
        dispatch(callApprovalsDeleteAPI({ approvalCodes }));
        window.location.reload();
      } else {
        return false;
      }
    } else {
      alert('삭제할 결재를 선택해주세요.');
    }
  }

  return (
    <div className={ApprovalStyle.outlet}>
      <h3>임시저장문서</h3>
      <div className={ApprovalStyle.draftInfo}>
        <img src={draft} className={ApprovalStyle.approvalImg} />
        <p>임시저장된 문서 보관함입니다.</p>
      </div>

      <ApprovalTable approvalList={approvalList}/>
      
      <div className={ApprovalStyle.pageButton}>
        {Array.isArray(approvalList) &&
          <button
            className={ApprovalStyle.activeButton}
            onClick={() => setCurrentPage(currentPage - 1)}
            disabled={currentPage === 1} > &lt; </button>
        }
        {pageNumber.map((num) => (
          <li key={num} onClick={() => setCurrentPage(num)}>
            <button
              className={ApprovalStyle.inactiveButton}
              style={currentPage === num ? { backgroundColor: '#E8EBE7' } : null} > {num} </button>
          </li>
        ))}
        {Array.isArray(approvalList) &&
          <button
            className={ApprovalStyle.activeButton}
            onClick={() => setCurrentPage(currentPage + 1)}
            disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
          > &gt; </button>
        }
      </div>
      
      <button onClick={onDeleteHandler}>삭제</button>
    </div>
  );
}

export default ApprovalStorage;