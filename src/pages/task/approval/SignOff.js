import Detail from '../../../pages/task/approval/Detail';
import { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { callUpdateAprroveYnAPI } from '../../../apis/ApprovalLineAPICalls';
import { callRejectApprovalAPI } from '../../../apis/ApprovalAPICalls';

function SignOff() {

  const dispatch = useDispatch();
  const params = useParams();
  const navigate = useNavigate();
  const loginInfo = useSelector(state => state.loginReducer);
  const lineList = useSelector(state => state.approvalLineReducer);

  const signOffOnApproval = () => {
    console.log(params.approvalCode);
    
    dispatch(callUpdateAprroveYnAPI({
      approvalCode: params.approvalCode,
      empCode: loginInfo.empCode
    }));
    
    window.location.reload();
  }

  const onRejectionHandler = () => {
    alert('반려!! 상태 코드 5로 변경')
    dispatch(callRejectApprovalAPI({
      approvalCode: params.approvalCode
    }));
  }

  const onDoneListHandler = () => {
    navigate('/task/approval/done');
  }

  return (
    <div>
      <h3>승인하기</h3>
      <Detail/>
      <button onClick={signOffOnApproval}>승인</button>
      <button onClick={onRejectionHandler}>반려</button>
      <button onClick={() => {navigate(-1)}}>뒤로가기</button>
      <button onClick={onDoneListHandler}>완료 목록 보러가기</button>
    </div>
  );
}

export default SignOff;