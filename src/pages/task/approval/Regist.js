import ApprovalStyle from '../../../css/Approval.module.css';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import ApprovalForm from '../../../components/task/Form';
import { callApprovalRegistAPI, callApprovalDraftAPI } from '../../../apis/ApprovalAPICalls';

import { useSelector, useDispatch } from 'react-redux';

function ApprovalRegist() {

  const [docType, setDocType] = useState('');
  const [isSelected, setIsSelected] = useState(false);
  const [form, setForm] = useState({});
  const [attachment, setAttachment] = useState(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [approvalLine, setApprovalLine] = useState([]);

  /* 양식 선택 핸들러 */
  const onClickHandler = (e) => {
    setDocType(e.target.innerText)
    setIsSelected(true);
  }

  /* 저장 버튼 핸들러 */
  const saveHandler = () => {
    const formData = new FormData();
    formData.append("empCode", form.empCode);
    formData.append("statusCode", 0); // 결재 상태를 0으로 저장
    formData.append("docType", form.docType);
    formData.append("approvalTitle", form.approvalTitle);
    formData.append("approvalContent", form.approvalContent);
    formData.append("oldFileName", form.oldFileName);
    formData.append("newFileName", form.newFileName);
    formData.append("link", form.link);
    formData.append("vacationType", form.vacationType);
    if (form.vacationStartDate) {
      formData.append("vacationStartDate", new Date(form.vacationStartDate));
    }
    if (form.vacationEndDate) {
      formData.append("vacationEndDate", new Date(form.vacationEndDate));
    }
    if (attachment) {
      formData.append("attachment", attachment);
    }
    // setIsSaved(!isSaved);

    console.log(form);
    console.log(approvalLine);
    dispatch(callApprovalRegistAPI({
      form: formData,
      approvalLineList: approvalLine
    }));
    alert("저장되었습니다");
    navigate('/task/approval/mylist');
    // navigate('/login');
  }

  const draftHandler = () => {
    const formData = new FormData();
    formData.append("empCode", form.empCode);
    formData.append("statusCode", 4); // 결재 상태를 4로 저장
    formData.append("docType", form.docType);
    formData.append("approvalTitle", form.approvalTitle);
    formData.append("approvalContent", form.approvalContent);
    formData.append("oldFileName", form.oldFileName);
    formData.append("newFileName", form.newFileName);
    formData.append("link", form.link);
    formData.append("vacationType", form.vacationType);
    if (form.vacationStartDate) {
      formData.append("vacationStartDate", new Date(form.vacationStartDate));
    }
    if (form.vacationEndDate) {
      formData.append("vacationEndDate", new Date(form.vacationEndDate));
    }
    if (attachment) {
      formData.append("attachment", attachment);
    }
    dispatch(callApprovalDraftAPI({
      form: formData,
      approvalLineList: approvalLine
    }));
    alert("임시보관함에 보관됩니다.");
    navigate('/task/approval/storage');
  }

  return (
    <div className={ApprovalStyle.outlet}>
      <div>
        <h3>기안서작성</h3>
      </div>

      <div className={ApprovalStyle.flex}>
        <div className={ApprovalStyle.docType}>
          <h4>기안서 양식을 선택하세요.</h4>
          <p>양식</p>
          <ul>
            <li onClick={onClickHandler}>품의서</li>
            <li onClick={onClickHandler}>지출결의서</li>
            <li onClick={onClickHandler}>입금확인서</li>
            <li onClick={onClickHandler}>전표</li>
            <li onClick={onClickHandler}>기안서</li>
            <li onClick={onClickHandler}>제안서</li>
            <li onClick={onClickHandler}>보고서</li>
          </ul>
          <p>기타</p>
          <ul>
            <li onClick={onClickHandler}>휴가신청서</li>
          </ul>

        </div>
        {
          isSelected &&
          <div>
            <ApprovalForm docType={docType} setIsSelected={setIsSelected} form={form} setForm={setForm} setAttachment={setAttachment} approvalLine={approvalLine} setApprovalLine={setApprovalLine} />

            <button onClick={saveHandler} className={ApprovalStyle.formButton}>저장</button>
            <button onClick={draftHandler}>임시저장</button>
          </div>
        }
      </div>


    </div>
  );
}

export default ApprovalRegist;