import ApprovalStyle from '../../../css/Approval.module.css';
import { useEffect, useRef, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { callApprovalDetailAPI, callApprovalDeleteAPI } from '../../../apis/ApprovalAPICalls';
import { callApprovalLineAPI } from '../../../apis/ApprovalLineAPICalls';
import StampBox from '../../../components/task/StampBox';

function ApprovalDetail() {

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useParams();
  const approvalDetail = useSelector(state => state.approvalReducer);
  const lineList = useSelector(state => state.approvalLineReducer);
  const loginInfo = useSelector(state => state.loginReducer);

  console.log(lineList);
  useEffect(
    () => {
      console.log('[ApprovalDetail] approvalCode : ', params.approvalCode);
      dispatch(callApprovalDetailAPI({
        approvalCode: params.approvalCode
      }));
      dispatch(callApprovalLineAPI({
        approvalCode: params.approvalCode
      }));
    }
    , []);

  /* 날짜 형식 */
  function formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = (d.getFullYear()) % 2000;
    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;
    return [year, month, day].join('/');
  }

  /* 삭제 버튼 핸들러 */
  const onDeleteHandler = () => {
    const answer = window.confirm( approvalDetail.approvalTitle+ '(을/를) 정말 삭제하시겠습니까?');
    if(answer === true) {
      dispatch(callApprovalDeleteAPI({	
        approvalCode: params.approvalCode
      })); 
    } else {
      return false;
    }    
		// navigate('/task/approval/list', { replace: true});
    navigate(-1);
  }

  /* 첨부파일 열기 */
  const fileView = () => {
    window.open(approvalDetail.newFileName, 'file', 'width= 500, height=500, left=400, top=100')
  }

  const test = () => {
    console.log(loginInfo)
  }

  return (
      <div className={ApprovalStyle.detailForm}>
        <div>
          <h2>{approvalDetail?.docType}</h2>
        </div>

        <div className={ApprovalStyle.flexEnd}>
          <div className={ApprovalStyle.docTable}>
            <table border={1}>
              <tbody>
                <tr>
                  <th>문서번호</th>
                  <td>{approvalDetail?.approvalCode}</td>
                </tr>
                <tr>
                  <th>부서</th>
                  <td>{approvalDetail?.emp?.dept?.deptName}</td>
                </tr>
                <tr>
                  <th>기안일</th>
                  <td>{formatDate(approvalDetail?.approvalDate)}</td>
                </tr>
                <tr>
                  <th>기안자</th>
                  <td>{approvalDetail?.emp?.empName}</td>
                </tr>
                <tr>
                  <th>직위</th>
                  <td>{approvalDetail?.emp?.position?.positionName}</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div className={ApprovalStyle.flex}>
            <div className={ApprovalStyle.stampHead}>
              <h4>결재</h4>
            </div>
            <div className={ApprovalStyle.horizontal}>
              {
                lineList.length > 0 && lineList.map((line) => (<StampBox key={line.lineCode} line={line} />))
              }
            </div>
          </div>
        </div>

        <div className={ApprovalStyle.contentTable}>
          <table border={1} className={ApprovalStyle.contentT}>
            <tbody>
              <tr>
                <th>제목</th>
                <td colSpan={3}>{approvalDetail?.approvalTitle}</td>
              </tr>
              {(approvalDetail?.docType === "휴가신청서") ?
                <>
                  <tr>
                    <th>휴가종류</th>
                    <td colSpan={3}>{approvalDetail?.vacationType}</td>
                  </tr>
                  <tr>
                    <th>시작일</th>
                    <td>{formatDate(approvalDetail?.vacationStartDate)}</td>
                    <th>종료일</th>
                    <td>{formatDate(approvalDetail?.vacationEndDate)}</td>
                  </tr>
                </> :
                <tr>
                  <th>첨부파일</th>
                  <td>
                    {(approvalDetail.oldFileName) ?
                      <p onClick={fileView}>{approvalDetail?.oldFileName}</p> :
                      <p style={{ color: 'gray' }}>첨부파일 없음</p>}
                  </td>
                </tr>}
              <tr>
                <th>내용</th>
                <td colSpan={3} style={(approvalDetail?.docType === "휴가신청서") && { height: 150 } || { height: 180 }}>{approvalDetail?.approvalContent}</td>
              </tr>
              <tr>
                <td colSpan={4} className={ApprovalStyle.space}>상기와 같이 {approvalDetail?.docType}(을/를) 제출하오니 첨부파일 확인 부탁드립니다.</td>
              </tr>
            </tbody>
          </table>

        </div>

                  {/* <div className={ApprovalStyle.approvalButton}>
            <button onClick={() => navigate(-1)}>닫기</button>
            {
              (loginInfo?.empCode === approvalDetail?.emp?.empCode) &&
              <button onClick={onDeleteHandler}>삭제</button>
            }
            <button onClick={test}>테스트</button>
            
          </div> */}
      </div>
  );
}

export default ApprovalDetail;