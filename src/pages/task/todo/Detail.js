import { useEffect, useState} from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import {
  callTodoDetailAPI,
  callTodoUpdateAPI
} from '../../../apis/TodoAPICalls';

function TodoDetail() {
  const dispatch = useDispatch();
  const params = useParams();
  const todoDetail = useSelector((state) => state.todoReducer);
  const navigate = useNavigate();


  const [modifyMode, setModifyMode] = useState(false);
 

  const [form, setForm] = useState({})

  


  useEffect(() => {
    console.log("[TodoUpdate] todoCode : ", params.todoCode);

    dispatch(
      callTodoDetailAPI({
        todoCode: params.todoCode,
      })
    );
  }, []);


   const onClickModifyModeHandler = () => {
    // 수정모드
    setModifyMode(true);
    setForm({
      todoCode: todoDetail.todoCode,
      // empCode: todoDetail.empCode,
      todoContent: todoDetail.todoContent,
      completeYn: todoDetail.completeYn,
      
      
    });
  };

  /* form 데이터 세팅 */
  const onChangeHandler = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const onClickTodoUpdateHandler = () => {
    console.log("[TodoUpdate] onClickTodoUpdateHandler");

    const formData = new FormData();
    formData.append("todoCode", form.todoCode);
    // formData.append("empCode", form.empCode);
    formData.append("todoContent", form.todoContent);
    formData.append("completeYn", form.completeYn);
    

 
    dispatch(
      callTodoUpdateAPI({
        // 상품 정보 업데이트
        form: formData,
      })
    );
    console.log(form);

    alert("업무을 수정했습니다.");
    navigate("/task/todo/list", { replace: true });
  };


  return (
      <div>
        {todoDetail && (
          <>
           <h4>업무세상</h4>

           <table border={1}>
            <tr>
              <th>업무내용</th>
              <th>업무상태</th>
              <th>업무종료일</th>
            </tr>
            <tr>
              <td>{todoDetail.todoContent}
              <input
                  type="text"
                  style={!modifyMode ? { backgroundColor: "gray" } : null}
                  name="todoContent"
                  placeholder="내용을 입력하세요"
                  value={
                    (!modifyMode
                      ? todoDetail.todoContent
                      : form.todoContent) || ""
                  }
                  onChange={onChangeHandler}
                  readOnly={modifyMode ? false : true}
                />
              
              </td>
              <td>{todoDetail.completeYn}
              <input
                  type="text"
                  style={!modifyMode ? { backgroundColor: "gray" } : null}
                  name="completeYn"
                  placeholder="내용을 입력하세요"
                  value={
                    (!modifyMode
                      ? todoDetail.completeYn
                      : form.completeYn) || ""
                  }
                  onChange={onChangeHandler}
                  readOnly={modifyMode ? false : true}
                />
              
              </td>
              <td>{todoDetail.todoDeadline}</td>
            </tr>


           </table>
           {!modifyMode && (
           <button onClick={onClickModifyModeHandler}>수정모드</button>
           )}
           {modifyMode && (
              <button onClick={onClickTodoUpdateHandler}>
                업무 수정 저장하기
              </button>
            )}
           </>

        )}
         
      </div>
  );
}

export default TodoDetail;