import '../../../css/Position.css'
import { useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { callTodoRegistAPI } from '../../../apis/TodoAPICalls';




function TodoRegist() {

  const dispatch = useDispatch();
	const navigate = useNavigate();
  const loginInfo = useSelector(state => state.loginReducer);

    const [form, setForm] = useState({
        todoContent: '',
        completeYn: '',
        todoDeadline: ''
    });

    const onChangeHandler = (e) => {
      setForm({
        ...form,
        [e.target.name]: e.target.value
      });
    };
  
      /* 등록 핸들러 */
    const onClickTodoRegistHandler = () => {
      console.log('[TodoRegist] onClickTodoRegistHandler');
  
      const formData = new FormData();
  
      formData.append("todoContent", form.todoContent);
      formData.append("completeYn", form.completeYn);
  
      if(form.todoDate) {
        formData.append("todoDate", new Date(form.todoDate));
      }
      if(form.todoDeadline) {
        formData.append("todoDeadline", new Date(form.todoDeadline));
      }
  
      dispatch(callTodoRegistAPI({
        form: formData,
        empCode:loginInfo.empCode
      }));
  
      console.log(form)
      alert('업무 등록 완료');
      navigate('/task/todo/list', {replace:true});
      window.location.reload();
    }
  
  
  return (
    <div className="positionRegist">
    <h4>업무등록</h4>
    <table border={1}>
    <thead style={{ backgroundColor: '#266666', color: '#ffffff'}}>
    <tr>
        <th>업무내용</th>
        <th>업무상태</th>
        <th>종료일자</th>
    </tr>
  </thead>
        <tr>
            <td>
                <input
                   name="todoContent"
                   placeholder="업무내용"
                   onChange={ onChangeHandler }
                  
                />
            </td>
            <td>
                <input
                   name="completeYn"
                   placeholder="업무상태"
                   onChange={ onChangeHandler }
                />
            </td>
            <td>
                <input
                   name="todoDeadline"
                   type="date"
                   placeholder="완료일자"
                   onChange={ onChangeHandler }
                   value={form.todoDeadline}
                />
            </td>
           
        </tr>
    </table>
    <button	className='mainButton' onClick={ onClickTodoRegistHandler }> 저장</button>
    <button className='subButton' onClick = { () => navigate(-1) }> 취소 </button>
</div>
  );
}

export default TodoRegist;