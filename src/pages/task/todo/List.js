import "../../../css/boardListStyle.css";

import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";

import { callTodoListAPI ,callTodoDetailAPI} from "../../../apis/TodoAPICalls";

function TodoList() {
  const dispatch = useDispatch();
  const todos = useSelector((state) => state.todoReducer);
  const todoList = todos.data;
  const navigate = useNavigate();
  const pageInfo = todos.pageInfo;
  const loginInfo = useSelector(state => state.loginReducer);

  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const [isTrue, setIsTrue] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [isMine, setIsMine] = useState(true);

  const pageNumber = [];
  if (pageInfo) {
    for (let i = 1; i <= pageInfo.pageEnd; i++) {
      pageNumber.push(i);
    }
  }

  const onSelectHandler = (e) => {
    setIsTrue(false);
    const input = document.getElementsByTagName("input");
    // console.log(input.length);
    for (var i = input.length - 1; i >= 0; i--) {
      if (input[0].checked === true) {
        input[i].checked = true;
      } else {
        input[i].checked = false;
      }
    }
  };

  /* 전체 조회 */
  useEffect(() => {
    dispatch(
      callTodoListAPI({
        currentPage: currentPage,
        empCode:loginInfo.empCode
      })
    );
  }, [loginInfo.empCode, currentPage]);

  /* 시간  */
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear() % 2000;

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("/");
  }

    /* 등록 */
    const onClickTodoRegist = () => {
      console.log("[TodoRegist] onClickTodoRegist");
      navigate("/task/todo/regist", { replace: false });
    };

    
  /* 수정 */
  const onClickTableTr = (todoCode) => {
    dispatch(
      callTodoDetailAPI({
        todoCode: todoCode,
      })
    );
    navigate(`/task/todo/detail/${todoCode}`, { replace: false });
  };

  return (
    <div className="boardlist">
      <div
        style={{
          listStyleType: "none",
          display: "flex",
          justifyContent: "left",
        }}
      >
        {Array.isArray(todoList) && (
          <button
            className="activeButton"
            onClick={() => setCurrentPage(currentPage - 1)}
            disabled={currentPage === 1}
          >
            &lt;
          </button>
        )}

        {pageNumber.map((num) => (
          <li key={num} onClick={() => setCurrentPage(num)}>
            <button
              className="inactiveButton"
              style={
                currentPage === num ? { backgroundColor: "#E8EBE7" } : null
              }
            >
              {num}
            </button>
          </li>
        ))}
        {Array.isArray(todoList) && (
          <button
            className="activeButton"
            onClick={() => setCurrentPage(currentPage + 1)}
            disabled={currentPage === pageInfo.pageEnd || pageInfo.total == 0}
          >
            &gt;
          </button>
        )}
      </div>

      <h3>업무 조회</h3>

      <table>
        <thead>
          <tr>
            <th style={{ width: 10 }}>
              
            </th>
            <th>게시일자</th>
            <th>마감일자</th>
            <th>업무내용</th>
            <th>완료여부</th>
            <th>사원 코드</th>
            <th>수정 테스트</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(todoList) &&
            todoList.map((todo) => (
              <tr key={todo.todoCode}>
                <td>
                  <input type="checkbox" />
                </td>

                {/* <td>{todo.emp?.empCode === 2 ? (formatDate(todo.todoDate)) : ''}</td>
                <td>{todo.emp?.empCode === 2 ?(formatDate (todo.todoDeadline)) : ''}</td>
                <td>{todo.emp?.empCode === 2 ? (todo.todoContent) : ''}</td>
                <td>{todo.emp?.empCode === 2 ?(todo.completeYn) : ''}</td>
                <td>{todo.emp?.empCode}</td> */}

                <td className="todoCode"
                  onClick={() => onClickTableTr(todo.todoCode)}>{formatDate(todo.todoDate)}</td>
                <td>{formatDate(todo.todoDeadline)}</td>
                <td>{todo.todoContent}</td>
                <td>{todo.completeYn}</td>
                <td>{todo.emp?.empCode}</td>
                <td><button>수정모드</button></td>
              </tr>
            ))}
        </tbody>
      </table>

      <button onClick={onClickTodoRegist}>등록</button>
      <button>완료</button>
    </div>
  );
}

export default TodoList;
