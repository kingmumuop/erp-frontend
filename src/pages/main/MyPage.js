import MyPageStyle from '../../css/MyPage.module.css';

function MyPage() {
  return (
    <div className={MyPageStyle.myPage}>
      <h3>개인정보관리</h3>
      <p>프로필 이미지</p>
      <p>이름</p>
      <p>입사일자</p>
      <p>전화</p>
      <p>휴대전화</p>
      <p>이메일</p>
      <p>주소</p>
      <p>계좌번호</p>

    </div>
  );
}

export default MyPage;