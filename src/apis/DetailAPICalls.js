
import { GET_STORAGE } from '../modules/DetailModule.js'


export const callStorageDetailAPI = ({storageCode}) => {
	const requestURL = `http://${process.env.REACT_APP_RESTAPI_IP}:7777/api/v1/storages/${storageCode}`;

	return async (dispatch, getState) => {

		const result = await fetch(requestURL, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Accept": "*/*",
				"Access-Control-Allow-Origin": "*"
				// "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
			}
		})
		.then(response => response.json());
		console.log('[StorageAPICalls] callStorageDetailAPI RESULT : ', result);
		if(result.status === 200){
			console.log('[StorageAPICalls] callStorageDeatilAPI SUCCESS');
			dispatch({ type: GET_STORAGE, payload: result.data });

		}
	};
}
